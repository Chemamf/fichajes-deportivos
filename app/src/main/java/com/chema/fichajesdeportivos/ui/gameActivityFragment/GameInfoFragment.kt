package com.chema.fichajesdeportivos.ui.gameActivityFragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.adapters.ListUserGameInfoAdapter
import com.chema.fichajesdeportivos.ui.GameActivity
import com.chema.fichajesdeportivos.viewModel.GameViewModel
import kotlinx.android.synthetic.main.dialog_delete_user_game.view.*
import kotlinx.android.synthetic.main.fragment_game_info.*
import java.text.SimpleDateFormat
import kotlin.math.roundToInt


class GameInfoFragment : Fragment() {

    private var gameViewModel: GameViewModel? = null
    private var isPasswordShowed = false
    private var listPreferredItemHeightSmall = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        gameViewModel = ViewModelProvider(requireActivity()).get(GameViewModel::class.java)
        gameViewModel!!.getMatchdaysAndMatches()

        //Volver a poner Livedatas por defecto
        gameViewModel!!.userToDeleteFromGamePositionLiveData.value = null
        gameViewModel!!.gameInfoUpdatedLiveData.value = false

        return inflater.inflate(R.layout.fragment_game_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i(ContentValues.TAG, requireActivity().toString())
        listPreferredItemHeightSmall = gameInfoListUsers.layoutParams.height
        (requireActivity() as GameActivity).enableViewsGame(false)
        setup()

    }

    private fun setup() {
        setTextViews()
        setButtonActions()
        userList()
        observers()
    }

    @SuppressLint("SimpleDateFormat")
    private fun setTextViews() {
        gameInfoGameName.text = gameViewModel!!.gameLiveData.value!!.name
        gameInfoPassword.text = requireContext().getString(R.string.game_info_password, "********")
        gameInfoLeague.text = requireContext().getString(
            R.string.game_info_league,
            gameViewModel!!.leagueLiveData.value!!.name
        )
        gameInfoCreationDate.text = requireContext().getString(
            R.string.game_info_date,
            SimpleDateFormat("dd/MM/yyyy").format(gameViewModel!!.gameLiveData.value!!.creationDate!!)
        )
        gameInfoNumberUsers.text = requireContext().getString(
            R.string.game_info_number_users,
            gameViewModel!!.gameLiveData.value!!.users.size,
            requireContext().resources.getInteger(R.integer.max_users_game)
        )
    }

    private fun userList() {
        val game = gameViewModel!!.gameLiveData.value!!
        //Tamaño de la lista
        if (game.users.size <= 4) {
            gameInfoListUsers.layoutParams.height =
                listPreferredItemHeightSmall * game.users.size
        } else {
            gameInfoListUsers.layoutParams.height =
                listPreferredItemHeightSmall * 4 + (listPreferredItemHeightSmall.toDouble() / 2).roundToInt()
        }
        val adapter = ListUserGameInfoAdapter(
            requireContext(), gameViewModel!!
        )
        gameInfoListUsers.adapter = adapter
    }

    private fun setButtonActions() {
        gameInfoShowPassword.setOnClickListener {
            if (!isPasswordShowed) {
                gameInfoPassword.text = requireContext().getString(
                    R.string.game_info_password,
                    gameViewModel!!.gameLiveData.value!!.password
                )
                isPasswordShowed = true
            } else {
                gameInfoPassword.text =
                    requireContext().getString(R.string.game_info_password, "********")
                isPasswordShowed = false
            }
        }
        gameInfoSpinnerIcon.setOnClickListener {
            showListUsers(false)
        }
        gameInfoLayoutUsers.setOnClickListener {
            showListUsers(false)
        }
        gameInfoFrameLayout.setOnClickListener {
            showListUsers(true)
        }
    }

    private fun showListUsers(isFrameLayout: Boolean) {
        if (!gameInfoListUsers.isVisible && !isFrameLayout) {
            gameInfoSpinnerIcon.animate().rotation(180F).setDuration(200L)
                .setInterpolator(LinearInterpolator()).start()
            gameInfoListUsers.visibility = View.VISIBLE
            gameInfoListUsers.animate().alpha(1.0f)
            gameInfoLastDivider.visibility = View.GONE
        } else if (gameInfoListUsers.isVisible) {
            gameInfoSpinnerIcon.animate().rotation(0F).setDuration(200L)
                .setInterpolator(LinearInterpolator()).start()
            gameInfoListUsers.animate().alpha(0.0f)
            gameInfoListUsers.visibility = View.GONE
            gameInfoLastDivider.visibility = View.VISIBLE
        }
    }

    private fun observers() {
        val isDataUpdatedObserver = Observer<Boolean> {
            if (it) {
                userList()
                gameInfoNumberUsers.text = requireContext().getString(
                    R.string.game_info_number_users,
                    gameViewModel!!.gameLiveData.value!!.users.size,
                    requireContext().resources.getInteger(R.integer.max_users_game)
                )
            }
        }
        gameViewModel!!.gameInfoUpdatedLiveData.observe(
            this.viewLifecycleOwner,
            isDataUpdatedObserver
        )

        val userToDeleteObserver = Observer<Int> {
            if (it != null) {
                showDialog(it)
            }
        }
        gameViewModel!!.userToDeleteFromGamePositionLiveData.observe(
            this.viewLifecycleOwner,
            userToDeleteObserver
        )

    }

    private fun showDialog(position: Int) {
        val dialog = Dialog(requireActivity())
        val dialogView =
            requireActivity().layoutInflater.inflate(R.layout.dialog_delete_user_game, null)
        dialog.setContentView(dialogView)

        val dialogMessage = dialogView.gameInfoDeleteUserMessage
        val deleteButton = dialogView.gameInfoDialogDeleteButton
        val cancelButton = dialogView.gameInfoDialogCancelButton

        dialogMessage.text = requireContext().getString(
            R.string.game_info_delete_account_message,
            gameViewModel!!.usersLiveData.value!![position].username
        )

        deleteButton.setOnClickListener {
            gameViewModel!!.deleteUser(position)
            dialog.dismiss()
        }

        cancelButton.setOnClickListener {
            dialog.cancel()
        }
        dialog.show()

    }


}