package com.chema.fichajesdeportivos.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.viewModel.factory.AccountSettingsViewModelFactory
import com.chema.fichajesdeportivos.model.UserProfile
import com.chema.fichajesdeportivos.viewModel.AccountSettingsViewModel
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_account_settings.*
import kotlinx.android.synthetic.main.dialog_delete_user.view.*


class AccountSettingsActivity : AppCompatActivity() {

    private val accountSettingsViewModel: AccountSettingsViewModel by viewModels {
        AccountSettingsViewModelFactory(
            application,
            intent.extras?.get("userAccount") as FirebaseUser,
            intent.extras?.get("userProfile") as UserProfile
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_settings)

        setSupportActionBar(toolBar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_24)

        setup()
    }

    private fun setup() {
        setUsername()
        settingsTextViewButtons()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onSupportNavigateUp(): Boolean {
        this.onBackPressed()
        return super.onSupportNavigateUp()
    }

    private fun setUsername() {
        username_textview.text = accountSettingsViewModel.userProfile.username
    }

    private fun settingsTextViewButtons() {
        changePassword.setOnClickListener {
            val accountSettingsIntent: Intent =
                Intent(this, ChangePasswordActivity::class.java).apply {
                    putExtra("userAccount", accountSettingsViewModel.userAccountLiveData.value)
                    putExtra(
                        "username", accountSettingsViewModel.userProfile.username
                    )
                }
            startActivity(accountSettingsIntent)
        }

        deleteAccount.setOnClickListener {
            showDialogFragment()
        }
    }

    private fun showDialogFragment() {
        val dialogBuilder = AlertDialog.Builder(this).create()
        val inflater: LayoutInflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.dialog_delete_user, null)

        val shake: Animation = AnimationUtils.loadAnimation(this, R.anim.shake)

        val password: EditText = dialogView.passwordEditText
        val deleteButton: TextView = dialogView.deleteAccountButton
        val cancelButton: TextView = dialogView.deleteAccountCancelButton

        deleteButton.setOnClickListener {
            deleteButton.isEnabled = false
            accountSettingsViewModel.deleteUser(password.text.toString())
        }

        val successfulObserver = Observer<Byte> {
            if (it != 1.toByte())
                deleteButton.isEnabled = true
            if (accountSettingsViewModel.successfulDeleteLiveData.value == 1.toByte()) {
                dialogBuilder.dismiss()
                startActivity(Intent(this, LoginActivity::class.java).apply { })
            } else if (accountSettingsViewModel.successfulDeleteLiveData.value == 2.toByte()) {
                password.setBackgroundResource(R.drawable.transparent_bg_bordered_edit_text_error)
                password.startAnimation(shake)
                password.setCompoundDrawablesWithIntrinsicBounds(
                    0, 0, R.drawable.ic_baseline_error_24, 0
                )
            }
        }
        accountSettingsViewModel.successfulDeleteLiveData.observe(this, successfulObserver)

        val messageObserver = Observer<String> {
            if (accountSettingsViewModel.successfulDeleteLiveData.value == 2.toByte() ||
                accountSettingsViewModel.successfulDeleteLiveData.value == 3.toByte()
            ) {
                Toast.makeText(
                    this@AccountSettingsActivity, it,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        accountSettingsViewModel.messageDeleteLiveData.observe(this, messageObserver)

        cancelButton.setOnClickListener {
            accountSettingsViewModel.successfulDeleteLiveData.value = 0
            dialogBuilder.cancel()
        }

        dialogBuilder.setView(dialogView)
        dialogBuilder.show()
    }

}