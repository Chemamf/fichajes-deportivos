package com.chema.fichajesdeportivos.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class InitialActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()

        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if (currentUser == null)
            showLogin()
        else
            showMain(currentUser)
    }

    private fun showMain(user: FirebaseUser) {
        val mainIntent: Intent = Intent(this, MainActivity::class.java).apply {
            putExtra("user", user)
        }
        startActivity(mainIntent)
    }

    private fun showLogin(){
        val loginIntent: Intent = Intent(this, LoginActivity::class.java).apply {  }
        startActivity(loginIntent)
    }
}