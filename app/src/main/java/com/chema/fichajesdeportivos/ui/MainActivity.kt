package com.chema.fichajesdeportivos.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.UserProfile
import com.chema.fichajesdeportivos.other.Utils.hideKeyboard
import com.chema.fichajesdeportivos.ui.mainActivityFragment.CreateGameFragment
import com.chema.fichajesdeportivos.ui.mainActivityFragment.GameListFragment
import com.chema.fichajesdeportivos.ui.mainActivityFragment.SearchGameFragment
import com.chema.fichajesdeportivos.viewModel.MainViewModel
import com.chema.fichajesdeportivos.viewModel.factory.MainViewModelFactory
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_drawer_header.view.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val mainViewModel: MainViewModel by viewModels {
        MainViewModelFactory(
            application,
            intent.extras?.get("user") as FirebaseUser
        )
    }
    private lateinit var toggle: ActionBarDrawerToggle


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolBar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        //Código para primera ejecución
        mainViewModel.firstRun()

        //Actualizar BD local si es necesario
        mainViewModel.updateLocalDB()

        //Seleccionar Lista de partidas por defecto
        onNavigationItemSelected(navigation_view.menu.getItem(0))

        setup()
    }

    private fun setup() {
        mainViewModel.getUserProfileFromAccount()
        setUsernameAndGameList()
        headerButtons()
        observers()
    }


    private fun enableViews(backButton: Boolean) {
        if (backButton) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            toggle.isDrawerIndicatorEnabled = false
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_24)
            toggle.setToolbarNavigationClickListener {
                val menuItem = navigation_view.menu.getItem(0)
                onNavigationItemSelected(menuItem)
            }
        } else {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            toggle = ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolBar,
                android.R.string.copy,
                android.R.string.selectAll
            )
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            drawerLayout.addDrawerListener(toggle)
            toggle.syncState()
            navigation_view.setNavigationItemSelectedListener(this)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var fragment: Fragment? = null
        val fragmentTransaction = supportFragmentManager.beginTransaction()

        val fragmentClass = when (item.itemId) {
            R.id.game_list -> {
                fragmentTransaction.setCustomAnimations(
                    R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right
                )
                hideKeyboard(this)
                enableViews(false)
                GameListFragment::class.java
            }
            R.id.create_game -> {
                fragmentTransaction.setCustomAnimations(
                    R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left
                )
                enableViews(true)
                CreateGameFragment::class.java
            }
            R.id.search_game -> {
                fragmentTransaction.setCustomAnimations(
                    R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left
                )
                enableViews(true)
                SearchGameFragment::class.java
            }
            else -> GameListFragment::class.java
        }

        try {
            fragment = fragmentClass.newInstance() as Fragment
        } catch (e: Exception) {
            e.printStackTrace()
        }

        fragmentTransaction.replace(R.id.fragment_container, fragment!!)
            .commit()
        item.isChecked = true
        drawerLayout.closeDrawers()

        return true
    }

    private fun observers() {
        val toastMessageObserver = Observer<String> {
            Toast.makeText(
                baseContext, it,
                Toast.LENGTH_SHORT
            ).show()
        }
        mainViewModel.toastLiveData.observe(this, toastMessageObserver)
    }

    private fun headerButtons() {
        val header = navigation_view.getHeaderView(0)

        header.logout_button.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed({
                FirebaseAuth.getInstance().signOut()
                val loginIntent: Intent = Intent(this, LoginActivity::class.java).apply {}
                startActivity(loginIntent)
                finish()
            }, 150)
            drawerLayout.closeDrawers()
        }

        header.edit_button.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed({
                val accountSettingsIntent: Intent =
                    Intent(this, AccountSettingsActivity::class.java).apply {
                        putExtra("userProfile", mainViewModel.userProfileLiveData.value)
                        putExtra("userAccount", mainViewModel.userAccountLiveData.value)
                    }
                startActivity(accountSettingsIntent)
            }, 150)
            drawerLayout.closeDrawers()
        }
    }

    private fun setUsernameAndGameList() {
        val usernameTextview = navigation_view.getHeaderView(0).username_textview
        val userProfileObserver = Observer<UserProfile> {
            if (it != null) {
                usernameTextview.text = it.username
                //Popular lista de partidas despues de obtener el perfil del usuario
                mainViewModel.populateGameList(false)
            }
        }
        mainViewModel.userProfileLiveData.observe(this, userProfileObserver)

    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else if (navigation_view.menu.getItem(0).isChecked) {
            finishAffinity()
        } else {
            onNavigationItemSelected(navigation_view.menu.getItem(0))
        }
    }


}