package com.chema.fichajesdeportivos.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.viewModel.LoginViewModel
import com.chema.fichajesdeportivos.viewModel.factory.LoginViewModelFactory
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_login.*
import java.time.LocalDateTime

class LoginActivity : AppCompatActivity() {

    private val loginViewModel: LoginViewModel by viewModels {
        LoginViewModelFactory(application)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setup()

    }

    private fun setup() {
        logIn()
        registration()
        observables()
    }

    private fun observables() {
        val toastObserver = Observer<String> {
            loginSignInButton.isEnabled = true
            Toast.makeText(
                baseContext, it,
                Toast.LENGTH_SHORT
            ).show()
        }
        loginViewModel.toastLiveData.observe(this, toastObserver)
    }

    private fun logIn() {
        loginSignInButton.setOnClickListener {
            loginSignInButton.isEnabled = false
            val username = loginEmailEditText.text!!.toString().trim()
            val password = loginPasswordEditText.text.toString()
            loginViewModel.login(username, password)
        }

        val loginObserver = Observer<FirebaseUser> {
            val mainIntent: Intent = Intent(this, MainActivity::class.java).apply {
                putExtra("user", it)
            }
            startActivity(mainIntent)
        }
        loginViewModel.userAccountLiveData.observe(this, loginObserver)
    }

    private fun registration() {
        loginSignUpButton.setOnClickListener {
            val registrationIntent = Intent(this, RegistrationActivity::class.java).apply { }
            startActivity(registrationIntent)
        }
    }

    override fun onBackPressed() {
        finishAffinity()
    }
}

