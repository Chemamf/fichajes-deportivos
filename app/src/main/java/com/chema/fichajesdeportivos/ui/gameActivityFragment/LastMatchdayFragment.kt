package com.chema.fichajesdeportivos.ui.gameActivityFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.adapters.ListMatchAdapter
import com.chema.fichajesdeportivos.ui.GameActivity
import com.chema.fichajesdeportivos.viewModel.GameViewModel
import kotlinx.android.synthetic.main.activity_game.*
import kotlinx.android.synthetic.main.fragment_last_matchday.*

class LastMatchdayFragment : Fragment() {
    private var gameViewModel: GameViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        gameViewModel = ViewModelProvider(requireActivity()).get(GameViewModel::class.java)

        //Volver a poner Livedatas por defecto

        return inflater.inflate(R.layout.fragment_last_matchday, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as GameActivity).enableViewsGame(false)
        setup()
    }

    private fun setup() {
        observers()
    }

    private fun observers() {
        if (gameViewModel?.matchdaysDataRetrieveLiveData?.value!!) {
            whenAllDataRetrieved()
        }
    }

    private fun whenAllDataRetrieved() {
        configList()
        setTextViews()
    }

    private fun configList() {
        matchesListView.adapter = ListMatchAdapter(
            requireContext(),
            gameViewModel!!.lastMatchesLiveData.value!!,
            gameViewModel!!.getTeams()
        )

        matchesListView.setOnItemClickListener { _, _, position, _ ->
            gameViewModel!!.currentMatchSelectedLiveData.value =
                gameViewModel!!.lastMatchesLiveData.value!![position]
            var fragment: Fragment? = null
            val fragmentTag = "MATCH_INFO"
            val fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(
                R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left,
                R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right
            )

            try {
                fragment = (MatchInfoFragment::class.java).newInstance() as Fragment
            } catch (e: Exception) {
                e.printStackTrace()
            }

            fragmentTransaction.replace(R.id.fragmentContainerGame, fragment!!, fragmentTag)
                .addToBackStack(null).commit()
            (requireActivity() as GameActivity).drawerLayoutGame.closeDrawers()

        }
    }

    private fun setTextViews() {
        matchesMatchdayTextview.text = getString(
            R.string.matchday_matches_list,
            gameViewModel!!.lastMatchdayLiveData.value!!.matchDay.toString()
        )

        matchesGameName.text = gameViewModel!!.gameLiveData.value!!.name
    }
}
