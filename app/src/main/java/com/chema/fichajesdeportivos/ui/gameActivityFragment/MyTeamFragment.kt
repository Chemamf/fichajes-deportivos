package com.chema.fichajesdeportivos.ui.gameActivityFragment

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.icu.util.Measure
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.widget.AutoCompleteTextView
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.adapters.DropdownFormationsAdapter
import com.chema.fichajesdeportivos.adapters.ListMyTeamLineUpAdapter
import com.chema.fichajesdeportivos.adapters.DropdownMyTeamReservesAdapter
import com.chema.fichajesdeportivos.adapters.ListAllPlayersAdapter
import com.chema.fichajesdeportivos.model.enum.Formation
import com.chema.fichajesdeportivos.model.enum.Position
import com.chema.fichajesdeportivos.other.Utils
import com.chema.fichajesdeportivos.ui.GameActivity
import com.chema.fichajesdeportivos.viewModel.GameViewModel
import kotlinx.android.synthetic.main.fragment_my_team.*
import kotlinx.android.synthetic.main.table_layout_my_team.view.*
import kotlin.math.ceil


class MyTeamFragment : Fragment() {
    private var gameViewModel: GameViewModel? = null
    private var dropdownShowed: AutoCompleteTextView? = null
    private var rowHeight: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        gameViewModel = ViewModelProvider(requireActivity()).get(GameViewModel::class.java)
        gameViewModel!!.setSavedPlayersMyTeam()

        //Volver a poner Livedatas por defecto
        gameViewModel!!.positionUpdatedLiveData.value = Position.DEFAULT
        gameViewModel!!.formationUpdatedLiveData.value = false

        return inflater.inflate(R.layout.fragment_my_team, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as GameActivity).enableViewsGame(false)
        setup()
    }

    private fun setup() {
        startViewsAndData()
        observers()
    }

    private fun startViewsAndData() {
        setRowHeight()
        myTeamGameName.text = gameViewModel!!.gameLiveData.value!!.name
        setButtonListeners()
        populateUserInfoTable()
        startSpinner()
        setPlayersViews(null)
        setAllPlayerLists()

    }

    //###########################################
    // Datos
    //###########################################
    @SuppressLint("SetTextI18n")
    private fun populateUserInfoTable() {
        myTeamTableData.removeAllViews()

        //Encabezado tabla
        val rowHead =
            layoutInflater.inflate(R.layout.table_layout_my_team, myTeamTableData, false)
        rowHead.myTeamTableCredit.text = context?.getString(R.string.actual_credit)
        rowHead.myTeamTableCredit.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.gray_textview
            )
        )
        rowHead.myTeamTableValue.text = context?.getString(R.string.team_value_complete)
        rowHead.myTeamTableValue.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.gray_textview
            )
        )
        rowHead.myTeamNumPlayers.text = context?.getString(R.string.players)
        rowHead.myTeamNumPlayers.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.gray_textview
            )
        )
        myTeamTableData.addView(rowHead)

        //Datos
        val rowData =
            layoutInflater.inflate(R.layout.table_layout_my_team, myTeamTableData, false)
        rowData.myTeamTableCredit.text =
            Utils.valueFormat(gameViewModel!!.userTeamLiveData.value!!.credit.toLong()) + "€"
        rowData.myTeamTableCredit.setTypeface(null, Typeface.BOLD)
        rowData.myTeamTableValue.text =
            gameViewModel!!.getTeamValue(gameViewModel!!.userTeamLiveData.value!!) + "€"
        rowData.myTeamTableValue.setTypeface(null, Typeface.BOLD)
        rowData.myTeamNumPlayers.text =
            gameViewModel!!.getNumberPlayersCurrent()
                .toString() + "/" + requireContext().resources.getInteger(R.integer.max_players_userteam)
                .toString()
        rowData.myTeamNumPlayers.setTypeface(null, Typeface.BOLD)

        myTeamTableData.addView(rowData)

    }

    //###########################################
    // Spinner buttons
    //###########################################
    private fun setButtonListeners() {
        myTeamSpinnerIcon.setOnClickListener {
            showSpinnerDropdown(myTeamFormationSpinner)
        }
        myTeamFormationSpinner.setOnClickListener {
            showSpinnerDropdown(myTeamFormationSpinner)
        }
        myTeamGKImageButton.setOnClickListener {
            showSpinnerDropdown(myTeamGKAddDropdown)
        }
        myTeamDFImageButton.setOnClickListener {
            showSpinnerDropdown(myTeamDFAddDropdown)
        }
        myTeamMFImageButton.setOnClickListener {
            showSpinnerDropdown(myTeamMFAddDropdown)
        }
        myTeamATImageButton.setOnClickListener {
            showSpinnerDropdown(myTeamATAddDropdown)
        }
        myTeamFrameLayout.setOnClickListener {
            closeSpinnerDropdown()
        }
        myTeamRelativeLayout.setOnClickListener {
            closeSpinnerDropdown()
        }
        myTeamFormationSpinner.doOnTextChanged { _, _, _, _ ->
            closeSpinnerDropdown()
        }
        myTeamSaveButton.setOnClickListener {
            gameViewModel!!.saveUserTeam()
        }
        myTeamUndoButton.setOnClickListener {
            gameViewModel!!.setSavedPlayersMyTeam()
            setPlayersViews(null)
            myTeamFormationSpinner.setText(
                gameViewModel!!.onEditUserTeamLiveData.value!!.first.formation,
                false
            )
        }
    }

    private fun showSpinnerDropdown(view: AutoCompleteTextView) {
        if (view != this.dropdownShowed) {
            closeSpinnerDropdown()
            view.showDropDown()
            if (view == myTeamFormationSpinner) {
                moveArrow(true)
            }
            this.dropdownShowed = view
        } else {
            view.dismissDropDown()
            if (view == myTeamFormationSpinner) {
                moveArrow(false)
            }
            this.dropdownShowed = null
        }
    }

    private fun closeSpinnerDropdown() {
        this.dropdownShowed?.dismissDropDown()
        if (this.dropdownShowed == myTeamFormationSpinner) {
            moveArrow(false)
        }
        this.dropdownShowed = null
    }

    //###########################################
    // Formacion
    //###########################################

    private fun startSpinner() {
        val formations = mutableListOf<String>()
        for (f in Formation.values()) {
            formations.add(f.formation)
        }
        val positon = gameViewModel!!.userTeamLiveData.value!!.formation.ordinal
        myTeamFormationSpinner.setAdapter(
            DropdownFormationsAdapter(
                requireContext(),
                gameViewModel!!
            )
        )
        myTeamFormationSpinner.setText(Formation.values()[positon].formation, false)
    }


    //###########################################
    // Alineacion
    //###########################################
    /**Si position es null, se actualzian todas las listas de todas las posiciones*/
    private fun setPlayersViews(position: Position?) {
        val formation = gameViewModel!!.onEditUserTeamLiveData.value!!.first
        closeSpinnerDropdown()
        if (position == Position.GOALKEEPER || position == null) {
            //Actualiza el adapter con los jugadores alineados actualmente y cambia su tamaño
            myTeamGKList.adapter = ListMyTeamLineUpAdapter(
                requireContext(),
                Position.GOALKEEPER,
                gameViewModel!!
            )
            myTeamGKList.layoutParams.height = rowHeight * myTeamGKList.count
            //Si se pueden añadir mas jugadores, actualizar el adapter de jugadores suplentes, cambia
            //el tamaño del dropdown de suplentes y hace que el botón de añadir sea visible.
            if (gameViewModel!!.onEditUserTeamLiveData.value!!.second[Position.GOALKEEPER].isNullOrEmpty()) {
                myTeamGKAddDropdown.setAdapter(
                    DropdownMyTeamReservesAdapter(
                        requireContext(),
                        Position.GOALKEEPER,
                        gameViewModel!!
                    )
                )
                myTeamGKAddDropdown.dropDownHeight = rowHeight * myTeamGKAddDropdown.adapter.count
                myTeamGKImageButton.visibility = View.VISIBLE
            } else {
                myTeamGKImageButton.visibility = View.GONE
            }
        }
        if (position == Position.DEFENDER || position == null) {
            myTeamDFList.adapter = ListMyTeamLineUpAdapter(
                requireContext(),
                Position.DEFENDER,
                gameViewModel!!
            )
            myTeamDFList.layoutParams.height = rowHeight * myTeamDFList.count
            if (gameViewModel!!.onEditUserTeamLiveData.value!!.second[Position.DEFENDER]!!.size < formation.defence) {
                myTeamDFAddDropdown.setAdapter(
                    DropdownMyTeamReservesAdapter(
                        requireContext(),
                        Position.DEFENDER,
                        gameViewModel!!
                    )
                )
                myTeamDFAddDropdown.dropDownHeight = rowHeight * myTeamDFAddDropdown.adapter.count
                myTeamDFImageButton.visibility = View.VISIBLE
            } else {
                myTeamDFImageButton.visibility = View.GONE
            }
        }
        if (position == Position.MIDFIELDER || position == null) {
            myTeamMFList.adapter = ListMyTeamLineUpAdapter(
                requireContext(),
                Position.MIDFIELDER,
                gameViewModel!!
            )
            myTeamMFList.layoutParams.height = rowHeight * myTeamMFList.count
            if (gameViewModel!!.onEditUserTeamLiveData.value!!.second[Position.MIDFIELDER]!!.size < formation.midfield) {
                myTeamMFAddDropdown.setAdapter(
                    DropdownMyTeamReservesAdapter(
                        requireContext(),
                        Position.MIDFIELDER,
                        gameViewModel!!
                    )
                )
                myTeamMFAddDropdown.dropDownHeight = rowHeight * myTeamMFAddDropdown.adapter.count
                myTeamMFImageButton.visibility = View.VISIBLE
            } else {
                myTeamMFImageButton.visibility = View.GONE
            }
        }
        if (position == Position.ATTACKER || position == null) {
            myTeamATList.adapter = ListMyTeamLineUpAdapter(
                requireContext(),
                Position.ATTACKER,
                gameViewModel!!
            )
            myTeamATList.layoutParams.height = rowHeight * myTeamATList.count
            if (gameViewModel!!.onEditUserTeamLiveData.value!!.second[Position.ATTACKER]!!.size < formation.forward) {
                myTeamATAddDropdown.setAdapter(
                    DropdownMyTeamReservesAdapter(
                        requireContext(),
                        Position.ATTACKER,
                        gameViewModel!!
                    )
                )
                myTeamATAddDropdown.dropDownHeight = rowHeight * myTeamATAddDropdown.adapter.count
                myTeamATImageButton.visibility = View.VISIBLE
            } else {
                myTeamATImageButton.visibility = View.GONE
            }
        }
    }

    //###########################################
    // Todos los jugadaores
    //###########################################

    private fun setAllPlayerLists() {
        val allPlayers = gameViewModel!!.getAllPlayers()
        if (allPlayers.first.isNotEmpty()) {
            myTeamPlayersList1.adapter = ListAllPlayersAdapter(
                requireContext(),
                allPlayers.first
            )
            val row1 = myTeamPlayersList1.adapter.getView(0, null, myTeamPlayersList1)
            row1.measure(
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
            )
            myTeamPlayersList1.layoutParams.height =
                row1.measuredHeight * (myTeamPlayersList1.count + 1)

            myTeamPlayersList2.adapter = ListAllPlayersAdapter(
                requireContext(),
                allPlayers.second
            )
            val row2 = myTeamPlayersList2.adapter.getView(0, null, myTeamPlayersList2)
            row2.measure(
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
            )
            myTeamPlayersList2.layoutParams.height =
                row2.measuredHeight * (myTeamPlayersList2.count + 1)
        }
    }


    //######################## Otros #######################

    private fun observers() {
        val updatedPositionObserver = Observer<Position> {
            if (it != Position.DEFAULT) {
                setPlayersViews(it)
            }
        }
        gameViewModel!!.positionUpdatedLiveData.observe(
            viewLifecycleOwner,
            updatedPositionObserver
        )

        val updatedFormationObserver = Observer<Boolean> {
            if (it) {
                setPlayersViews(null)
                myTeamFormationSpinner.setText(
                    gameViewModel!!.onEditUserTeamLiveData.value!!.first.formation,
                    false
                )
            }
        }
        gameViewModel!!.formationUpdatedLiveData.observe(
            viewLifecycleOwner,
            updatedFormationObserver
        )
    }


    // ################ Métodos auxiliares #################

    private fun moveArrow(open: Boolean) {
        if (open) {
            myTeamSpinnerIcon.animate().rotation(180F).setDuration(200L)
                .setInterpolator(LinearInterpolator()).start()
        } else {
            myTeamSpinnerIcon.animate().rotation(0F).setDuration(200L)
                .setInterpolator(LinearInterpolator()).start()
        }
    }

    private fun setRowHeight() {
        rowHeight = ceil(
            TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                42F,
                resources.displayMetrics
            ).toDouble()
        ).toInt()
    }
}
