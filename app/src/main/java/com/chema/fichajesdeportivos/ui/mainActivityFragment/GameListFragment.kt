package com.chema.fichajesdeportivos.ui.mainActivityFragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.Game
import com.chema.fichajesdeportivos.adapters.ListSubItemAdapter
import com.chema.fichajesdeportivos.ui.GameActivity
import com.chema.fichajesdeportivos.viewModel.MainViewModel
import kotlinx.android.synthetic.main.fragment_game_list.*


class GameListFragment : Fragment() {

    private var mainViewModel: MainViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mainViewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)

        //Volver a poner LiveDatas por defecto

        return inflater.inflate(R.layout.fragment_game_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    private fun setup() {
        observers()
        openGame()
    }

    private fun openGame() {
        gameListView.setOnItemClickListener { _, _, position, _ ->
            val gameSelected = mainViewModel?.gameListLiveData?.value?.get(position)
            val gameIntent = Intent(requireActivity(), GameActivity::class.java).apply {
                putExtra("gameId", gameSelected?.id)
                putExtra("userProfile", mainViewModel?.userProfileLiveData?.value)
            }
            startActivity(gameIntent)
        }
    }

    private fun observers() {
        val gameListObserver = Observer<List<Game>> {
            if (it != null) {
                val itemList: MutableList<String> = mutableListOf()
                val subItemList: MutableList<String> = mutableListOf()
                for (game in it) {
                    itemList.add(game.name)
                    subItemList.add(
                        "Jugadores: " + game.users.size.toString() + "/" + context?.resources?.getInteger(
                            R.integer.max_users_game
                        ).toString()
                    )
                }
                val adapter = ListSubItemAdapter(
                    requireActivity().applicationContext,
                    itemList, subItemList
                )
                gameListView.adapter = adapter
            }
        }
        mainViewModel?.gameListLiveData?.observe(viewLifecycleOwner, gameListObserver)
    }


}