package com.chema.fichajesdeportivos.ui.gameActivityFragment

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.ui.GameActivity
import com.chema.fichajesdeportivos.viewModel.GameViewModel
import kotlinx.android.synthetic.main.fragment_classification.*
import kotlinx.android.synthetic.main.table_row_classification.view.*

class ClassificationFragment : Fragment() {

    private var gameViewModel: GameViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        gameViewModel = ViewModelProvider(requireActivity()).get(GameViewModel::class.java)

        //Volver a poner Livedatas por defecto

        return inflater.inflate(R.layout.fragment_classification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as GameActivity).enableViewsGame(false)
        setup()
    }

    private fun setup() {
        observerAllDataRetrieve()
    }

    private fun observerAllDataRetrieve() {
        if (gameViewModel?.gameDataRetrieveLiveData?.value!!) {
            whenAllDataRetrieved()
        } else {
            val isDataRetrieved = Observer<Boolean> {
                if (it) {
                    whenAllDataRetrieved()
                }
            }
            gameViewModel?.gameDataRetrieveLiveData?.observe(viewLifecycleOwner, isDataRetrieved)
        }
    }

    private fun whenAllDataRetrieved() {
        populateClassification()
        setGameName()
    }

    @SuppressLint("SetTextI18n")
    private fun populateClassification() {
        classificationTable.removeAllViews()
        //Encabezado de tabla
        val rowHead =
            layoutInflater.inflate(R.layout.table_row_classification, classificationTable, false)
        rowHead.user.text = context?.getString(R.string.user)
        rowHead.value.text = context?.getString(R.string.team_value)
        rowHead.pointsLmd.text = context?.getString(R.string.points_last_match)
        rowHead.points.text = context?.getString(R.string.points)
        rowHead.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.black))
        classificationTable.addView(rowHead)

        //Filas de tabla
        for (i in gameViewModel!!.usersLiveData.value!!.indices) {
            val row: View = layoutInflater.inflate(
                R.layout.table_row_classification,
                classificationTable,
                false
            )
            row.setBackgroundColor(
                if ((i + 1) % 2 == 0) ContextCompat.getColor(
                    requireContext(),
                    R.color.dark_background
                ) else ContextCompat.getColor(requireContext(), R.color.dark_gray)
            )
            row.position.text = (i + 1).toString()
            row.user.text = gameViewModel!!.usersLiveData.value!![i].username
            row.value.text =
                gameViewModel!!.getTeamValue(gameViewModel!!.userTeamsLiveData.value!![i]) + "€"
            row.pointsLmd.text =
                gameViewModel!!.userTeamsLiveData.value!![i].pointsLastMatch.toString()
            row.points.text = gameViewModel!!.userTeamsLiveData.value!![i].totalPoints.toString()
            //Si es el usuario que ha inciado sesión, la fila aparece en negrita
            if (gameViewModel!!.usersLiveData.value!![i].id.equals(gameViewModel!!.userLiveData.value!!.id)) {
                row.position.setTypeface(null, Typeface.BOLD)
                row.user.setTypeface(null, Typeface.BOLD)
                row.value.setTypeface(null, Typeface.BOLD)
                row.pointsLmd.setTypeface(null, Typeface.BOLD)
                row.points.setTypeface(null, Typeface.BOLD)
            }
            classificationTable.addView(row)
        }
    }

    private fun setGameName() {
        classificationGameName.text = gameViewModel!!.gameLiveData.value!!.name
    }
}