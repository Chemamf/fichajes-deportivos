package com.chema.fichajesdeportivos.ui


import android.content.Intent
import android.os.Bundle
import android.text.Editable
import androidx.lifecycle.Observer
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.doAfterTextChanged
import kotlinx.android.synthetic.main.activity_registration.*
import androidx.activity.viewModels
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.viewModel.RegistrationViewModel
import com.chema.fichajesdeportivos.viewModel.factory.RegistrationViewModelFactory
import java.util.*

class RegistrationActivity : AppCompatActivity() {

    private val registrationViewModel: RegistrationViewModel by viewModels {
        RegistrationViewModelFactory(application)
    }

    // Local es true al cambiar el editText, es necesario ya que LiveData ejecutaria codigo no
    // necesario cada vez que se escribe o borra en el editText
    private var invalidUsernameLocal = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        // Initialize Firebase Auth
        setup()
    }

    private fun setup() {
        registration()
        checkUsername()
        changeUsernameEditText()
        changeEmailEditText()
        observables()
        cancel()
    }

    private fun registration() {
        registrationSignUpButton.setOnClickListener {
            registrationSignUpButton.isEnabled = false
            val username = registrationUsernameEditText.text.toString().trim().toLowerCase()
            val email = registrationEmailEditText.text.toString().trim()
            val password = registrationPasswordEditText.text.toString()
            val repeatPassword = registrationRepeatPasswordEditText.text.toString()
            registrationViewModel.registration(
                username,
                email,
                password,
                repeatPassword,
                invalidUsernameLocal
            )
        }
    }

    private fun cancel() {
        registrationCancelButton.setOnClickListener {
            onBackPressed()
        }
    }

    private fun observables() {
        val successfulObserver = Observer<Boolean> {
            if (it) {
                val mainIntent: Intent =
                    Intent(this, MainActivity::class.java).apply {
                        putExtra("user", registrationViewModel.userAccountLiveData.value)
                    }
                startActivity(mainIntent)
                finish()
            } else {
                registrationSignUpButton.isEnabled = true
            }
        }
        registrationViewModel.successLiveData.observe(this, successfulObserver)

        val toastObserver = Observer<String> {
            Toast.makeText(
                baseContext, it,
                Toast.LENGTH_SHORT
            ).show()
        }
        registrationViewModel.toastLiveData.observe(this, toastObserver)
    }

    //Cambiar editTexts

    private fun checkUsername() {
        registrationUsernameEditText.addTextChangedListener(
            object : TextWatcher {
                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                private var timer: Timer = Timer()
                private val DELAY: Long = 1500 // milliseconds
                override fun afterTextChanged(s: Editable) {
                    timer.cancel()
                    timer = Timer()
                    timer.schedule(
                        object : TimerTask() {
                            override fun run() {
                                registrationViewModel.checkUsernameAux(
                                    registrationUsernameEditText.text.toString().toLowerCase()
                                )
                            }
                        },
                        DELAY
                    )
                }
            }
        )
    }


    private fun changeUsernameEditText() {
        registrationUsernameEditText.doAfterTextChanged {
            if (!invalidUsernameLocal)
                invalidUsernameLocal = true
            registrationUsernameEditTextLayout.isErrorEnabled = false
            registrationUsernameEditTextLayout.helperText = null
            if (registrationUsernameEditText.text!!.isBlank())
                usernameVerificationProgress.visibility = View.GONE
            else
                usernameVerificationProgress.visibility = View.VISIBLE
            registrationUsernameEditTextLayout.setBoxStrokeColorStateList(
                ContextCompat.getColorStateList(
                    baseContext,
                    R.color.text_edit_layout_outline_color
                )!!
            )
        }

        val invalidUsernameObserver = Observer<Boolean> {
            usernameVerificationProgress.visibility = View.GONE
            invalidUsernameLocal = registrationViewModel.invalidUsernameLiveData.value!!
            if (registrationUsernameEditText.text!!.isNotBlank()) {
                if (registrationViewModel.invalidUsernameLiveData.value!!)
                    registrationUsernameEditTextLayout.error =
                        getString(R.string.repeated_username)
                else {
                    registrationUsernameEditTextLayout.setBoxStrokeColorStateList(
                        ContextCompat.getColorStateList(
                            baseContext,
                            R.color.text_edit_layout_outline_color_green
                        )!!
                    )
                    registrationUsernameEditTextLayout.helperText =
                        getString(R.string.valid_username)
                }
            }
        }
        registrationViewModel.invalidUsernameLiveData.observe(this, invalidUsernameObserver)
    }

    private fun changeEmailEditText() {
        registrationEmailEditText.doAfterTextChanged {
            if (registrationEmailEditTextLayout.isErrorEnabled) {
                registrationEmailEditTextLayout.isErrorEnabled = false
            } else if (registrationEmailEditText.text.toString()
                    .equals(registrationViewModel.invalidEmailLiveData.value) &&
                registrationEmailEditText.text.toString().isNotBlank()
            ) {
                registrationEmailEditTextLayout.error = getString(R.string.repeated_email)
            }
        }

        val invalidemailObserver = Observer<String> {
            if (it.isNotEmpty())
                registrationEmailEditTextLayout.error = getString(R.string.repeated_email)
        }
        registrationViewModel.invalidEmailLiveData.observe(this, invalidemailObserver)
    }
}