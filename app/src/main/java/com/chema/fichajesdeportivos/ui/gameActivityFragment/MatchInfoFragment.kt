package com.chema.fichajesdeportivos.ui.gameActivityFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.adapters.ListPlayerAdapter
import com.chema.fichajesdeportivos.ui.GameActivity
import com.chema.fichajesdeportivos.viewModel.GameViewModel
import kotlinx.android.synthetic.main.fragment_match_info.*

class MatchInfoFragment : Fragment() {

    private var gameViewModel: GameViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        gameViewModel = ViewModelProvider(requireActivity()).get(GameViewModel::class.java)
        gameViewModel!!.getMatchdaysAndMatches()

        //Volver a poner Livedatas por defecto

        return inflater.inflate(R.layout.fragment_match_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as GameActivity).enableViewsGame(true)
        setup()
    }

    private fun setup() {
        setPlayersList()
        setTextViews()
    }

    private fun setPlayersList() {
        val teamsAndPlayers = gameViewModel!!.getPlayersMatch()
        matchInfoHomeTeam.adapter = ListPlayerAdapter(requireContext(), teamsAndPlayers[0].second)
        matchInfoAwayTeam.adapter = ListPlayerAdapter(requireContext(), teamsAndPlayers[1].second)
        matchInfoHomeTeamName.text = teamsAndPlayers[0].first.longName
        matchInfoAwayTeamName.text = teamsAndPlayers[1].first.longName
        matchInfoHomeTeamPoints.text =
            getString(R.string.count_points, teamsAndPlayers[0].third.toString())
        matchInfoAwayTeamPoints.text =
            getString(R.string.count_points, teamsAndPlayers[1].third.toString())
    }

    private fun setTextViews() {
        matchInfoMatchdayTextview.text = getString(
            R.string.matchday_matches_list,
            gameViewModel!!.lastMatchdayLiveData.value!!.matchDay.toString()
        )
        matchInfoGameName.text = gameViewModel!!.gameLiveData.value!!.name
    }
}