package com.chema.fichajesdeportivos.ui

import android.content.ContentValues
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.UserProfile
import com.chema.fichajesdeportivos.other.Utils
import com.chema.fichajesdeportivos.ui.gameActivityFragment.*
import com.chema.fichajesdeportivos.viewModel.GameViewModel
import com.chema.fichajesdeportivos.viewModel.factory.GameViewModelFactory
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_game.*
import kotlinx.android.synthetic.main.activity_game_drawer_header.view.*
import kotlinx.android.synthetic.main.activity_game_drawer_header.view.edit_button
import kotlinx.android.synthetic.main.activity_game_drawer_header.view.logout_button

class GameActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val gameViewModel: GameViewModel by viewModels {
        GameViewModelFactory(
            application,
            intent.extras?.get("userProfile") as UserProfile
        )
    }
    private lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        setSupportActionBar(toolBarGame)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        //Seleccionar Clasificación por defecto
        onNavigationItemSelected(navigationViewGame.menu.getItem(0))

        setup()
    }

    private fun setup() {
        gameViewModel.getGameAndData(intent.extras?.get("gameId") as String)
        observers()
        headerButtons()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var fragment: Fragment? = null
        var fragmentTag = ""
        val fragmentTransaction = supportFragmentManager.beginTransaction().setCustomAnimations(
            R.anim.anim_slide_in_right,
            R.anim.anim_slide_out_right,
        )

        val fragmentClass = when (item.itemId) {
            R.id.classification -> {
                Utils.hideKeyboard(this)
                fragmentTag = "CLASSIFICATION"
                ClassificationFragment::class.java
            }
            R.id.lastMatchday -> {
                Utils.hideKeyboard(this)
                fragmentTag = "LAST_MATCHDAY"
                LastMatchdayFragment::class.java
            }
            R.id.gameInfo ->{
                Utils.hideKeyboard(this)
                fragmentTag = "GAME_INFO"
                GameInfoFragment::class.java
            }
            R.id.myTeam ->{
                Utils.hideKeyboard(this)
                fragmentTag = "MY_TEAM"
                MyTeamFragment::class.java
            }
            R.id.market ->{
                Utils.hideKeyboard(this)
                fragmentTag = "MARKET"
                MarketFragment::class.java
            }
            else -> ClassificationFragment::class.java
        }

        try {
            fragment = fragmentClass.newInstance() as Fragment
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(ContentValues.TAG, e.message, e)
        }

        fragmentTransaction.replace(R.id.fragmentContainerGame, fragment!!, fragmentTag)
            .commit()
        item.isChecked = true
        drawerLayoutGame.closeDrawers()

        return true
    }

    fun enableViewsGame(backButton: Boolean) {
        if (backButton) {
            drawerLayoutGame.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            toggle.isDrawerIndicatorEnabled = false
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_24)
            toggle.setToolbarNavigationClickListener {
                supportFragmentManager.popBackStackImmediate()
            }
        } else {
            drawerLayoutGame.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            toggle = ActionBarDrawerToggle(
                this,
                drawerLayoutGame,
                toolBarGame,
                android.R.string.copy,
                android.R.string.selectAll
            )
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            drawerLayoutGame.addDrawerListener(toggle)
            toggle.syncState()
            navigationViewGame.setNavigationItemSelectedListener(this)
        }
    }

    private fun observers() {
        val isDataRetrievedObserver = Observer<Boolean> {
            if (it) {
                setUsername()
                gameViewModel.getMatchdaysAndMatches()
            }
        }
        gameViewModel.gameDataRetrieveLiveData.observe(this, isDataRetrievedObserver)

        val isMatchdayDataRetrievedObserver = Observer<Boolean> {
            if (it) {
                gameViewModel.updateMarket()
            }
        }
        gameViewModel.matchdaysDataRetrieveLiveData.observe(this, isMatchdayDataRetrievedObserver)

        val toastMessageObserver = Observer<String> {
            Toast.makeText(
                baseContext, it,
                Toast.LENGTH_SHORT
            ).show()
        }
        gameViewModel.toastLiveData.observe(this, toastMessageObserver)
    }

    private fun setUsername() {
        val usernameTextView = navigationViewGame.getHeaderView(0).usernameTextview
        usernameTextView.text = gameViewModel.userLiveData.value!!.username

    }

    private fun headerButtons() {
        val header = navigationViewGame.getHeaderView(0)

        header.logout_button.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed({
                FirebaseAuth.getInstance().signOut()
                val loginIntent: Intent = Intent(this, LoginActivity::class.java).apply {}
                startActivity(loginIntent)
                finish()
            }, 150)
            drawerLayoutGame.closeDrawers()
        }

        header.edit_button.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed({
                val accountSettingsIntent: Intent =
                    Intent(this, AccountSettingsActivity::class.java).apply {
                        putExtra("userProfile", gameViewModel.userLiveData.value)
                        putExtra("userAccount", FirebaseAuth.getInstance().currentUser)
                    }
                startActivity(accountSettingsIntent)
            }, 150)
            drawerLayoutGame.closeDrawers()
        }
    }


}