package com.chema.fichajesdeportivos.ui.mainActivityFragment

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.Game
import com.chema.fichajesdeportivos.ui.MainActivity
import com.chema.fichajesdeportivos.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_search_game.*
import java.util.*


class SearchGameFragment : Fragment() {

    private var mainViewModel: MainViewModel? = null
    private var gameSelected: Game? = null
    private var oldPositionSelected: Int? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mainViewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)

        //Volver a poner LiveDatas por defecto
        mainViewModel?.gamesFoundLiveData?.value = mutableListOf()
        mainViewModel?.successSearchGame?.value = false
        mainViewModel?.successJoinGame?.value = false
        mainViewModel?.wrongPasswordLiveData?.value = false


        return inflater.inflate(R.layout.fragment_search_game, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    private fun setup() {
        searchGames()
        selectGame()
        joinGame()
    }

    private fun searchGames() {
        searchGameButton.setOnClickListener {
            gameSelected = null
            searchGameButton.isEnabled = false
            searchGameListView.adapter = null
            mainViewModel?.searchGames(
                searchGameEditText.text.toString().trim().toLowerCase(Locale.ROOT)
            )
        }

        val gamesFoundObserver = Observer<List<Game>> {
            if (it != null) {
                val list: MutableList<String> = mutableListOf()
                if (it.isNotEmpty()) {
                    for (game in it) {
                        list.add(game.name)
                    }
                }
                val adapter = ArrayAdapter(
                    requireActivity().applicationContext,
                    R.layout.listview_item_custom,
                    list
                )
                searchGameListView.adapter = adapter
            }
        }
        mainViewModel?.gamesFoundLiveData?.observe(viewLifecycleOwner, gamesFoundObserver)

        val successSearchObserver = Observer<Boolean> {
            searchGameButton.isEnabled = true
        }
        mainViewModel?.successSearchGame?.observe(viewLifecycleOwner, successSearchObserver)

        //Al presioanr enter en searchEditText, buscar partidas
        searchGameEditText.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                searchGameButton.performClick()
                return@OnKeyListener true
            }
            false
        })
    }

    private fun selectGame() {
        searchGameListView.setOnItemClickListener { _, view, position, _ ->
            gameSelected = mainViewModel?.gamesFoundLiveData?.value?.get(position)

            //Quita la seleccion del ultimo item seleccionado
            if (oldPositionSelected != null) {
                searchGameListView.get(oldPositionSelected!!).setBackgroundColor(
                    ContextCompat.getColor(
                        requireActivity().applicationContext,
                        R.color.transparent
                    )
                )
            }
            //Cambia la position antigua por la actual para ser la próxima en borrarse
            oldPositionSelected = position

            //Selecciona el nuevo item
            view.setBackgroundColor(
                ContextCompat.getColor(
                    requireActivity().applicationContext,
                    R.color.green_25
                )
            )
        }
    }

    private fun joinGame() {
        joinGameButton.setOnClickListener {
            joinGameButton.isEnabled = false
            mainViewModel?.joinGame(gameSelected, searchGamePasswordEditText.text.toString())
            val wrongPasswordObserver = Observer<Boolean> {
                if (it) {
                    val shake: Animation =
                        AnimationUtils.loadAnimation(
                            requireActivity().applicationContext,
                            R.anim.shake
                        )
                    searchGamePasswordEditTextLayout.startAnimation(shake)
                    searchGamePasswordEditTextLayout.error = getString(R.string.wrong_password)
                    searchGamePasswordEditText.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, R.drawable.ic_baseline_error_24, 0
                    )
                } else {
                    searchGamePasswordEditTextLayout.isErrorEnabled = false
                    searchGamePasswordEditText.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, 0, 0
                    )
                }
            }
            mainViewModel?.wrongPasswordLiveData?.observe(viewLifecycleOwner, wrongPasswordObserver)
        }

        val successJoinObserver = Observer<Boolean> {
            if (it) {
                val menuItem = requireActivity().navigation_view.menu.getItem(0)
                mainViewModel?.populateGameList(true)
                (activity as MainActivity).onNavigationItemSelected(menuItem)
            } else
                joinGameButton.isEnabled = true
        }
        mainViewModel?.successJoinGame?.observe(viewLifecycleOwner, successJoinObserver)
    }
}