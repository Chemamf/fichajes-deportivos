package com.chema.fichajesdeportivos.ui.gameActivityFragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.adapters.DropdownCustomAdapter
import com.chema.fichajesdeportivos.model.Player
import com.chema.fichajesdeportivos.other.Utils
import com.chema.fichajesdeportivos.ui.GameActivity
import com.chema.fichajesdeportivos.viewModel.GameViewModel
import com.google.firebase.firestore.DocumentReference
import kotlinx.android.synthetic.main.fragment_add_offer.*


class AddOfferFragment : Fragment() {

    private var gameViewModel: GameViewModel? = null
    private var players = mutableListOf<Player>()
    private var playerSelected: Player? = null
    private var isDropdownShowing = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        gameViewModel = ViewModelProvider(requireActivity()).get(GameViewModel::class.java)

        //Volver a poner Livedatas por defecto
        gameViewModel!!.resetOfferSuccess()

        return inflater.inflate(R.layout.fragment_add_offer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as GameActivity).enableViewsGame(true)
        setup()
    }

    private fun setup() {
        setPlayers()
        setViews()
        observers()
    }

    private fun setPlayers() {
        val playersRef = mutableListOf<DocumentReference>()
        for (p in gameViewModel!!.userTeamLiveData.value!!.playersLineUp) {
            if (!gameViewModel!!.marketLiveData.value!!.any { it.player!!.id.equals(p.id) }) {
                playersRef.add(p)
            }
        }
        for (p in gameViewModel!!.userTeamLiveData.value!!.playersReserves) {
            if (!gameViewModel!!.marketLiveData.value!!.any { it.player!!.id.equals(p.id) }) {
                playersRef.add(p)
            }
        }
        players =
            gameViewModel!!.getPlayers(Utils.docRefListToIdList(playersRef)) as MutableList<Player>
    }

    private fun setViews() {
        addOfferGameName.text = gameViewModel!!.gameLiveData.value!!.name
        setPlayerDropdownAdapter()
        addOfferValue.text = requireContext().getString(R.string.player_value, " - ")
        setPriceSizeDropdown()
        setAddButton()
    }

    private fun setPlayerDropdownAdapter() {
        val playerNames = players.map { it.name }
        addOfferPlayerDropdown.setAdapter(
            DropdownCustomAdapter(
                requireContext(),
                playerNames,
                R.layout.spinner_dropdown_item_autocomplete_custom
            )
        )
        addOfferPlayerDropdown.setOnClickListener {
            showDropdown(false)
        }
        addOfferSpinnerIcon.setOnClickListener {
            showDropdown(false)
        }
        addOfferPlayerDropdown.doOnTextChanged { text, _, _, _ ->
            showDropdown(true)
            playerSelected = players[playerNames.indexOf(text.toString())]
            addOfferValue.text = requireContext().getString(
                R.string.player_value,
                Utils.valueFormat(playerSelected!!.value.toLong())
            )
        }
    }

    private fun setPriceSizeDropdown() {
        addOfferPriceSize.setText("K")
        addOfferPriceSize.setAdapter(
            DropdownCustomAdapter(
                requireContext(),
                listOf("K", "M"),
                R.layout.spinner_dropdown_item_center_custom
            )
        )
        addOfferPriceSize.setOnClickListener {
            addOfferPriceSize.showDropDown()
        }
    }

    private fun setAddButton() {
        addOfferAddButton.setOnClickListener {
            gameViewModel!!.addOffer(
                playerSelected,
                addOfferPriceEditText.text.toString(),
                addOfferPriceSize.text.toString()
            )
        }
    }

    private fun showDropdown(onlyClose: Boolean) {
        if (isDropdownShowing || onlyClose) {
            addOfferSpinnerIcon.animate().rotation(0F).setDuration(200L)
                .setInterpolator(LinearInterpolator()).start()
            addOfferPlayerDropdown.dismissDropDown()
            isDropdownShowing = false
        } else {
            addOfferSpinnerIcon.animate().rotation(180F).setDuration(200L)
                .setInterpolator(LinearInterpolator()).start()
            addOfferPlayerDropdown.showDropDown()
            isDropdownShowing = true
        }
    }

    private fun observers() {
        val offerSuccessObserver = Observer<Byte> {
            if (it == 3.toByte()) {
                requireActivity().supportFragmentManager.popBackStack()
            } else if (it == 13.toByte()) {
                gameViewModel!!.toastLiveData.value = getString(R.string.add_offer_error)
            }
        }
        gameViewModel!!.offerActionSuccessLiveData.observe(viewLifecycleOwner, offerSuccessObserver)
    }
}