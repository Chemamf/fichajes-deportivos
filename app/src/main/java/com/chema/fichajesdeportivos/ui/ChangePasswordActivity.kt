package com.chema.fichajesdeportivos.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.viewModel.factory.ChangePasswordViewModelFactory
import com.chema.fichajesdeportivos.viewModel.ChangePasswordViewModel
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.activity_change_password.toolBar

class ChangePasswordActivity : AppCompatActivity() {

    private val changePasswordViewModel: ChangePasswordViewModel by viewModels {
        ChangePasswordViewModelFactory(
            application,
            intent.extras?.get("userAccount") as FirebaseUser,
            intent.extras?.get("username") as String
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)

        setSupportActionBar(toolBar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_24)

        setup()
    }

    private fun setup() {
        setUsername()
        cancel()
        updatePassword()
        showToast()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Log.i("TIRA: ", "tira nene")
        finish()
    }

    override fun onSupportNavigateUp(): Boolean {
        this.onBackPressed()
        return super.onSupportNavigateUp()
    }

    private fun setUsername() {
        usernameTextview.text = changePasswordViewModel.userProfileLiveData.value
    }

    private fun cancel() {
        updateCancelButton.setOnClickListener {
            this.onBackPressed()
        }
    }

    private fun updatePassword() {
        updateSignUpButton.setOnClickListener {
            updateSignUpButton.isEnabled = false
            changePasswordViewModel.updatePassword(
                updateActualPasswordEditText.text.toString(),
                updateNewPasswordEditText.text.toString(),
                updateRepeatNewPasswordEditText.text.toString()
            )
        }
        val successfulObserver = Observer<Boolean> {
            if (it) {
                startActivity(Intent(this, LoginActivity::class.java).apply { })
                finish()
            } else {
                updateSignUpButton.isEnabled = true
            }
        }
        changePasswordViewModel.successfulLiveData.observe(this, successfulObserver)
    }

    private fun showToast() {
        val messageObserver = Observer<String> {
            Toast.makeText(
                baseContext, it,
                Toast.LENGTH_SHORT
            ).show()
        }
        changePasswordViewModel.messageLiveData.observe(this, messageObserver)
    }
}