package com.chema.fichajesdeportivos.ui.gameActivityFragment

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.adapters.DropdownCustomAdapter
import com.chema.fichajesdeportivos.adapters.ListOffersAdapter
import com.chema.fichajesdeportivos.model.Offer
import com.chema.fichajesdeportivos.model.Player
import com.chema.fichajesdeportivos.other.Utils
import com.chema.fichajesdeportivos.ui.GameActivity
import com.chema.fichajesdeportivos.viewModel.GameViewModel
import kotlinx.android.synthetic.main.activity_game.*
import kotlinx.android.synthetic.main.dialog_bid_offer.view.*
import kotlinx.android.synthetic.main.dialog_do_offer.view.*
import kotlinx.android.synthetic.main.fragment_market.*

class MarketFragment : Fragment() {

    private var gameViewModel: GameViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        gameViewModel = ViewModelProvider(requireActivity()).get(GameViewModel::class.java)

        //Volver a poner Livedatas por defecto
        gameViewModel!!.offerPlayerLiveData.value = null
        gameViewModel!!.resetOfferSuccess()

        return inflater.inflate(R.layout.fragment_market, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as GameActivity).enableViewsGame(false)
        setup()
    }

    private fun setup() {
        startViewsAndData()
        setButtonsListeners()
        generateDialogs()
    }

    private fun startViewsAndData() {
        marketGameName.text = gameViewModel!!.gameLiveData.value!!.name
        marketCredit.text =
            getString(
                R.string.actual_credit_value,
                Utils.valueFormat(gameViewModel!!.userTeamLiveData.value!!.credit.toLong())
            )
        if (gameViewModel!!.marketRetrievedLiveData.value!!) {
            setMarketLists()
        } else {
            val marketRetrievedObserver = Observer<Boolean> {
                if (it) {
                    setMarketLists()
                }
            }
            gameViewModel!!.marketRetrievedLiveData.observe(
                viewLifecycleOwner,
                marketRetrievedObserver
            )
        }
    }

    private fun setMarketLists() {
        val marketLists = gameViewModel!!.getMarketLists()
        val allRealTeams = gameViewModel!!.getAllRealTeams()
        marketOffersList1.adapter = ListOffersAdapter(
            requireContext(),
            marketLists.first[0],
            marketLists.second,
            allRealTeams,
            gameViewModel!!
        )
        val row1 = marketOffersList1.adapter.getView(0, null, marketOffersList1)
        row1.measure(
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        )
        marketOffersList1.layoutParams.height =
            (row1.measuredHeight * (marketOffersList1.count + 0.2)).toInt()


        marketOffersList2.adapter = ListOffersAdapter(
            requireContext(),
            marketLists.first[1],
            marketLists.second,
            allRealTeams,
            gameViewModel!!
        )
        val row2 = marketOffersList2.adapter.getView(0, null, marketOffersList2)
        row2.measure(
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        )
        marketOffersList2.layoutParams.height =
            (row2.measuredHeight * (marketOffersList2.count + 0.2)).toInt()
    }

    private fun setButtonsListeners() {
        marketAddPlayer.setOnClickListener {
            var fragment: Fragment? = null
            val fragmentTag = "ADD_OFFER"
            val fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(
                R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left,
                R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right
            )

            try {
                fragment = (AddOfferFragment::class.java).newInstance() as Fragment
            } catch (e: Exception) {
                e.printStackTrace()
            }

            fragmentTransaction.replace(R.id.fragmentContainerGame, fragment!!, fragmentTag)
                .addToBackStack(null).commit()
            (requireActivity() as GameActivity).drawerLayoutGame.closeDrawers()
        }

        marketBidsReceived.setOnClickListener {
            var fragment: Fragment? = null
            val fragmentTag = "USER_BIDDING"
            val fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(
                R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left,
                R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right
            )

            try {
                fragment = (ManageOffersFragment::class.java).newInstance() as Fragment
            } catch (e: Exception) {
                e.printStackTrace()
            }

            fragmentTransaction.replace(R.id.fragmentContainerGame, fragment!!, fragmentTag)
                .addToBackStack(null).commit()
            (requireActivity() as GameActivity).drawerLayoutGame.closeDrawers()
        }
    }

    //###########################################
    // Dialogs
    //###########################################

    //Primer boolean: Comprar/vender, Segundo boolean: Ya existe oferta del usuario/no existe
    private fun generateDialogs() {
        val offerPlayerObserver = Observer<Triple<Boolean, Player, Offer>> {
            if (it != null) {
                if (it.first) {
                    if (gameViewModel!!.offerToBuy()) {
                        deleteBidDialog()
                    } else {
                        doOfferDialog()
                    }
                } else {
                    deleteOwnOfferDialog()
                }
            }
        }
        gameViewModel!!.offerPlayerLiveData.observe(viewLifecycleOwner, offerPlayerObserver)
    }

    private fun doOfferDialog() {
        val dialog = Dialog(requireActivity())
        val dialogView =
            requireActivity().layoutInflater.inflate(R.layout.dialog_do_offer, null)
        dialog.setContentView(dialogView)

        val title = dialogView.doOfferTitle
        val value = dialogView.doOfferValue
        val price = dialogView.doOfferPrice
        val amount = dialogView.doOfferAmount
        val amountSize = dialogView.doOfferAmountSize
        val accept = dialogView.doOfferAcceptButton
        val cancel = dialogView.doOfferCancelButton

        title.text = requireContext().getString(
            R.string.offer_for_value,
            gameViewModel!!.offerPlayerLiveData.value!!.second.name
        )

        value.text = requireContext().getString(
            R.string.player_value,
            Utils.valueFormat(gameViewModel!!.offerPlayerLiveData.value!!.second.value.toLong())
        )

        price.text = requireContext().getString(
            R.string.price_value,
            Utils.valueFormat(gameViewModel!!.offerPlayerLiveData.value!!.third.price.toLong())
        )

        amountSize.setText("K")
        amountSize.setAdapter(
            DropdownCustomAdapter(
                requireContext(),
                listOf("K", "M"),
                R.layout.spinner_dropdown_item_center_custom
            )
        )
        amountSize.setOnClickListener {
            amountSize.showDropDown()
        }

        accept.setOnClickListener {
            gameViewModel!!.doOffer(amount.text.toString(), amountSize.text.toString())
            val offerSuccesObserver = Observer<Byte> {
                if (it == 1.toByte()) {
                    dialog.dismiss()
                } else if (it == 11.toByte()) {
                    amount.setBackgroundResource(R.drawable.transparent_bg_bordered_edit_text_error)
                    gameViewModel!!.toastLiveData.value =
                        requireContext().getString(R.string.do_offer_error)
                }
            }
            gameViewModel!!.offerActionSuccessLiveData.observe(
                viewLifecycleOwner,
                offerSuccesObserver
            )
        }

        cancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun deleteBidDialog() {
        val dialog = Dialog(requireActivity())
        val dialogView =
            requireActivity().layoutInflater.inflate(R.layout.dialog_bid_offer, null)
        dialog.setContentView(dialogView)

        val text = dialogView.deleteOfferText
        val accept = dialogView.deleteOfferAcceptButton
        val cancel = dialogView.deleteOfferCancelButton

        text.text = requireContext().getString(
            R.string.delete_offer_message_value,
            Utils.valueFormat(
                gameViewModel!!.offerPlayerLiveData.value!!.third.bidding[gameViewModel!!
                    .userLiveData.value!!.id]!!.toLong()
            ),
            gameViewModel!!.offerPlayerLiveData.value!!.second.name
        )

        accept.setOnClickListener {
            gameViewModel!!.deleteBid()
            val offerSuccesObserver = Observer<Byte> {
                if (it == 2.toByte()) {
                    dialog.dismiss()
                } else if (it == 12.toByte()) {
                    gameViewModel!!.toastLiveData.value =
                        requireContext().getString(R.string.delete_offer_error)
                }
            }
            gameViewModel!!.offerActionSuccessLiveData.observe(
                viewLifecycleOwner,
                offerSuccesObserver
            )
        }

        cancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun deleteOwnOfferDialog() {
        val dialog = Dialog(requireActivity())
        val dialogView =
            requireActivity().layoutInflater.inflate(R.layout.dialog_bid_offer, null)
        dialog.setContentView(dialogView)

        val text = dialogView.deleteOfferText
        val accept = dialogView.deleteOfferAcceptButton
        val cancel = dialogView.deleteOfferCancelButton

        text.text = requireContext().getString(
            R.string.delete_own_offer_message_value,
            gameViewModel!!.offerPlayerLiveData.value!!.second.name
        )

        accept.setOnClickListener {
            gameViewModel!!.deleteOffer()
            val offerSuccesObserver = Observer<Byte> {
                if (it == 4.toByte()) {
                    setMarketLists()
                    dialog.dismiss()
                } else if (it == 14.toByte()) {
                    gameViewModel!!.toastLiveData.value =
                        requireContext().getString(R.string.delete_offer_error)
                }
            }
            gameViewModel!!.offerActionSuccessLiveData.observe(
                viewLifecycleOwner,
                offerSuccesObserver
            )
        }

        cancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()

    }

}