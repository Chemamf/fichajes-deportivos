package com.chema.fichajesdeportivos.ui.mainActivityFragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.League
import com.chema.fichajesdeportivos.ui.MainActivity
import com.chema.fichajesdeportivos.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_create_game.*
import java.util.*


class CreateGameFragment : Fragment() {

    private var mainViewModel: MainViewModel? = null

    // Local es true al cambiar el editText, es necesario ya que LiveData ejecutaria codigo no
    // necesario cada vez que se escribe o borra en el editText
    private var invalidGameNameLocal = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mainViewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)
        mainViewModel?.populateLeaguesList()

        //Volver a poner LiveDatas por defecto
        mainViewModel!!.successGameCreated.value = false
        mainViewModel!!.invalidGameNameLiveData.value = true

        return inflater.inflate(R.layout.fragment_create_game, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    private fun setup() {
        observers()
        changeNameEditText()
        checkGameName()
        createGame()
    }

    private fun observers() {
        val leagueListObserver = Observer<List<League>> {
            if (!it.isNullOrEmpty()) {
                val list: MutableList<String> = mutableListOf()
                for (league in it) {
                    list.add(league.name)
                }

                val adapter = ArrayAdapter(
                    requireActivity().applicationContext,
                    R.layout.spinner_item_custom,
                    list
                )
                adapter.setDropDownViewResource(R.layout.spinner_dropdown_item_custom)
                createGameLeagueSpinner.adapter = adapter
            }
        }
        mainViewModel?.leaguesListLiveData?.observe(viewLifecycleOwner, leagueListObserver)
    }

    private fun createGame() {
        createGameCreateButton.setOnClickListener {
            createGameCreateButton.isEnabled = false
            mainViewModel?.createGame(
                createGameNameEditText.text.toString().trim().toLowerCase(Locale.ROOT),
                createGamePasswordEditText.text.toString(),
                mainViewModel?.leaguesListLiveData?.value?.get(createGameLeagueSpinner.selectedItemPosition)!!.id,
                invalidGameNameLocal
            )
        }

        val successObserver = Observer<Boolean> {
            if (it) {
                val menuItem = requireActivity().navigation_view.menu.getItem(0)
                mainViewModel?.populateGameList(true)
                (activity as MainActivity).onNavigationItemSelected(menuItem)
            } else {
                createGameCreateButton.isEnabled = true
            }
        }
        mainViewModel?.successGameCreated?.observe(viewLifecycleOwner, successObserver)
    }


    //Cambiar editTexts

    private fun checkGameName() {
        createGameNameEditText.addTextChangedListener(
            object : TextWatcher {
                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                private var timer: Timer = Timer()
                private val DELAY: Long = 1500 // milliseconds
                override fun afterTextChanged(s: Editable) {
                    timer.cancel()
                    timer = Timer()
                    timer.schedule(
                        object : TimerTask() {
                            override fun run() {
                                mainViewModel!!.checkGameNameAux(
                                    createGameNameEditText.text.toString().toLowerCase()
                                )
                            }
                        },
                        DELAY
                    )
                }
            }
        )
    }

    private fun changeNameEditText() {
        createGameNameEditText.doAfterTextChanged {
            if (!invalidGameNameLocal)
                invalidGameNameLocal = true
            createGameNameEditTextLayout.isErrorEnabled = false
            createGameNameEditTextLayout.helperText = null
            if (createGameNameEditText.text!!.isBlank())
                GameNameVerificationProgress.visibility = View.GONE
            else
                GameNameVerificationProgress.visibility = View.VISIBLE
            createGameNameEditTextLayout.setBoxStrokeColorStateList(
                ContextCompat.getColorStateList(
                    requireActivity().applicationContext,
                    R.color.text_edit_layout_outline_color
                )!!
            )
        }

        val invalidGameNameObserver = Observer<Boolean> {
            GameNameVerificationProgress.visibility = View.GONE
            invalidGameNameLocal = mainViewModel!!.invalidGameNameLiveData.value!!
            if (createGameNameEditText.text!!.isNotBlank()) {
                if (mainViewModel!!.invalidGameNameLiveData.value!!)
                    createGameNameEditTextLayout.error =
                        getString(R.string.repeated_game_name)
                else {
                    createGameNameEditTextLayout.setBoxStrokeColorStateList(
                        ContextCompat.getColorStateList(
                            requireActivity().applicationContext,
                            R.color.text_edit_layout_outline_color_green
                        )!!
                    )
                    createGameNameEditTextLayout.helperText =
                        getString(R.string.valid_game_name)
                }
            }
        }
        mainViewModel!!.invalidGameNameLiveData.observe(viewLifecycleOwner, invalidGameNameObserver)
    }
}