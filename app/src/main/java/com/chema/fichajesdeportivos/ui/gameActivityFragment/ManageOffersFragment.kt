package com.chema.fichajesdeportivos.ui.gameActivityFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.adapters.ListOffersAdapter
import com.chema.fichajesdeportivos.adapters.ListOffersReceivedAdapter
import com.chema.fichajesdeportivos.model.Offer
import com.chema.fichajesdeportivos.model.Player
import com.chema.fichajesdeportivos.other.Utils
import com.chema.fichajesdeportivos.ui.GameActivity
import com.chema.fichajesdeportivos.viewModel.GameViewModel
import com.google.firebase.firestore.DocumentReference
import kotlinx.android.synthetic.main.fragment_manage_offers.*

class ManageOffersFragment : Fragment() {

    private var gameViewModel: GameViewModel? = null
    private var userOffers = mutableListOf<Offer>()
    private var playersUserOffers = mutableListOf<Player>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        gameViewModel = ViewModelProvider(requireActivity()).get(GameViewModel::class.java)

        //Volver a poner Livedatas por defecto
        gameViewModel!!.resetOfferSuccess()

        return inflater.inflate(R.layout.fragment_manage_offers, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as GameActivity).enableViewsGame(true)
        setup()
    }

    private fun setup() {
        setPlayersAndUserOffers()
        manageOffersGameName.text = gameViewModel!!.gameLiveData.value!!.name
        setList()
        observers()
    }

    private fun setPlayersAndUserOffers() {
        userOffers.clear()
        playersUserOffers.clear()
        val playerRefs = mutableListOf<DocumentReference>()
        for (o in gameViewModel!!.marketLiveData.value!!) {
            if (o.seller!!.id.equals(gameViewModel!!.userLiveData.value!!.id) && o.bidding.isNotEmpty()) {
                userOffers.add(o)
                playerRefs.add(o.player!!)
            }
        }
        playersUserOffers.addAll(gameViewModel!!.getPlayers(Utils.docRefListToIdList(playerRefs)))
    }

    private fun setList() {
        manageOffersList.adapter = ListOffersReceivedAdapter(
            requireContext(),
            userOffers,
            playersUserOffers,
            gameViewModel!!.usersLiveData.value!!,
            gameViewModel!!
        )
    }

    private fun observers() {
        val offerSuccessObserver = Observer<Byte> {
            if (it == 5.toByte() || it == 6.toByte()) {
                if (it == 5.toByte())
                    setPlayersAndUserOffers()
                setList()
            } else if (it == 15.toByte()) {
                gameViewModel!!.toastLiveData.value = getString(R.string.accept_offer_error)
            } else if (it == 16.toByte()) {
                gameViewModel!!.toastLiveData.value = getString(R.string.denny_offer_error)
            }
        }
        gameViewModel!!.offerActionSuccessLiveData.observe(viewLifecycleOwner, offerSuccessObserver)
    }
}