package com.chema.fichajesdeportivos.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.DocumentReference

data class Offer(
    @DocumentId val id: String = "",
    var price: Double = 0.0,
    //Seller es userProfile, no userTeam
    var seller: DocumentReference? = null,
    var bidding: Map<String,Double> = mutableMapOf(),
    var matchDay: String = "",
    var player: DocumentReference? = null
)
