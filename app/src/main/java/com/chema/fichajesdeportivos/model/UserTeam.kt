package com.chema.fichajesdeportivos.model

import com.chema.fichajesdeportivos.model.enum.Formation
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.DocumentReference
import java.io.Serializable

data class UserTeam(
    @DocumentId val id: String = "",
    var formation: Formation = Formation.F442,
    var credit: Double = 0.0,
    var pointsLastMatch: Int = 0,
    var totalPoints: Int = 0,
    var userProfile: DocumentReference? = null,
    var playersLineUp: MutableList<DocumentReference> = mutableListOf(),
    var playersReserves: MutableList<DocumentReference> = mutableListOf()
) : Serializable
