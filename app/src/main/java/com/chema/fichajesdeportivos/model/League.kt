package com.chema.fichajesdeportivos.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.DocumentReference

data class League(
    @DocumentId val id: String = "",
    var name: String = "",
    var country: String = "",
    var realTeams : MutableList<DocumentReference> = mutableListOf(),
    var matchDay : MutableList<DocumentReference> = mutableListOf()
)