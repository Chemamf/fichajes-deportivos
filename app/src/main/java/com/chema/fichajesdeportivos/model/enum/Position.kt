package com.chema.fichajesdeportivos.model.enum

enum class Position {
    DEFAULT, GOALKEEPER, DEFENDER, MIDFIELDER, ATTACKER
}