package com.chema.fichajesdeportivos.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.DocumentReference
import java.io.Serializable

data class RealTeam(
    @DocumentId val id: String = "",
    var name: String = "",
    var longName: String = "",
    var players : List<DocumentReference> = mutableListOf()
) : Serializable
