package com.chema.fichajesdeportivos.model.enum

enum class Formation(val formation: String, val defence: Int, val midfield: Int, val forward: Int) {
    F541("5-4-1", 5, 4, 1),
    F532("5-3-2", 5, 3, 2),
    F523("5-2-3", 5, 2, 3),
    F451("4-5-1", 4, 5, 1),
    F442("4-4-2", 4, 4, 2),
    F433("4-3-3", 4, 3, 3),
    F424("4-2-4", 4, 2, 4),
    F361("3-6-1", 3, 6, 1),
    F352("3-5-2", 3, 5, 2),
    F343("3-4-3", 3, 4, 3);

    override fun toString(): String {
        return name
    }
}