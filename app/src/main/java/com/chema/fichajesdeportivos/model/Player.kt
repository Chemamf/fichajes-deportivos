package com.chema.fichajesdeportivos.model

import com.chema.fichajesdeportivos.model.enum.Position
import com.google.firebase.firestore.DocumentId
import java.io.Serializable

data class Player(
    @DocumentId val id: String = "",
    var name: String = "",
    var totalPoints : Int = 0,
    var pointsLastMatch : Int = 0,
    var position : Position = Position.DEFAULT,
    var value : Int = 0
) : Serializable
