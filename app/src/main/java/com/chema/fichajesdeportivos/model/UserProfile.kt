package com.chema.fichajesdeportivos.model

import com.google.firebase.firestore.DocumentId
import java.io.Serializable

data class UserProfile(
    @DocumentId val id: String = "",
    var username: String = "",
    var userAccountUID: String = "",
) : Serializable