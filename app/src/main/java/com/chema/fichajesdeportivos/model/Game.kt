package com.chema.fichajesdeportivos.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.DocumentReference
import java.io.Serializable
import java.util.*

data class Game(
    @DocumentId val id: String = "",
    var name: String = "",
    var password: String = "",
    var creationDate: Date? = null,
    var league: DocumentReference? = null,
    var administrator: DocumentReference? = null,
    var users: MutableList<DocumentReference> = mutableListOf(),
    var market: MutableList<DocumentReference> = mutableListOf(),
    var userTeams: MutableList<DocumentReference> = mutableListOf()
) : Serializable