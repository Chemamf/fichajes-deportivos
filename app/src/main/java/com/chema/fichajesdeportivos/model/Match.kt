package com.chema.fichajesdeportivos.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.DocumentReference
import java.io.Serializable

data class Match(
    @DocumentId val id: String = "",
    var homeTeamGoals: Int? = null,
    var awayTeamGoals: Int? = null,
    var homeTeam: DocumentReference? = null,
    var awayTeam: DocumentReference? = null
) : Serializable
