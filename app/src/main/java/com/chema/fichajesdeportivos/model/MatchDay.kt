package com.chema.fichajesdeportivos.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.DocumentReference
import java.io.Serializable
import java.util.*

data class MatchDay(
    @DocumentId val id: String = "",
    var matchDay: Int = 0,
    var startMarket: Date? = null,
    var startMatchPeriod: Date? = null,
    var endDate: Date? = null,
    var matches: MutableList<DocumentReference> = mutableListOf()
) : Serializable
