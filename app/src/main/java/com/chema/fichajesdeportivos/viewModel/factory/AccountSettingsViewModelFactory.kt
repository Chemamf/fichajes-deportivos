package com.chema.fichajesdeportivos.viewModel.factory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.chema.fichajesdeportivos.model.UserProfile
import com.chema.fichajesdeportivos.viewModel.AccountSettingsViewModel
import com.google.firebase.auth.FirebaseUser


class AccountSettingsViewModelFactory(
    private val application: Application,
    private val userAccount: FirebaseUser,
    private val userProfile: UserProfile
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AccountSettingsViewModel(application, userAccount, userProfile) as T
    }
}