package com.chema.fichajesdeportivos.viewModel

import android.app.Application
import android.content.ContentValues
import android.content.SharedPreferences
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.*
import com.chema.fichajesdeportivos.model.enum.Position
import com.chema.fichajesdeportivos.other.Utils
import com.chema.fichajesdeportivos.repository.*
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentReference
import java.sql.Timestamp
import java.util.*

class MainViewModel(application: Application, user: FirebaseUser) : AndroidViewModel(application) {

    //Instancias de repositorios
    private val userProfileRepository = UserProfileRepository.instance
    private val leagueRepository = LeagueRepository.instance
    private val gameRepository = GameRepository.instance
    private val realTeamRepository = RealTeamRepository.instance
    private val playerRepository = PlayerRepository.instance
    private val localDB = DB(getApplication<Application>().applicationContext)
    private val systemVariablesRepository = SystemVariablesRepository.instance
    private val matchDayRepository = MatchDayRepository.instance
    private val matchRepository = MatchRepository.instance
    private val userTeamRepository = UserTeamRepository.instance


    //LiveDatas
    val userAccountLiveData = MutableLiveData(user)
    val userProfileLiveData = MutableLiveData<UserProfile>()
    val toastLiveData = MutableLiveData<String>()

    //Lista de partidas
    val gameListLiveData = MutableLiveData<List<Game>>()

    //Crear partida
    val leaguesListLiveData = MutableLiveData<List<League>>()
    val successGameCreated = MutableLiveData<Boolean>()
    val invalidGameNameLiveData = MutableLiveData(true)

    //Buscar partida
    val gamesFoundLiveData = MutableLiveData<List<Game>>()
    val successSearchGame = MutableLiveData<Boolean>()
    val successJoinGame = MutableLiveData<Boolean>()
    val wrongPasswordLiveData = MutableLiveData<Boolean>()

    //Código para primera ejecución
    fun firstRun() {
        val preferences: SharedPreferences =
            getApplication<Application>().applicationContext.getSharedPreferences(
                "preferences",
                AppCompatActivity.MODE_PRIVATE
            )
        val firstStart = preferences.getBoolean("firstStart", true)
        if (firstStart) {
            //Crear preferencias
            val editor: SharedPreferences.Editor = preferences.edit()
            editor.putString("versionDB", "0.0")
            editor.putBoolean("firstStart", false)
            editor.apply()
        }
    }

    //Actualziar BD Local si es necesario
    fun updateLocalDB() {
        var isDBUpdated = false
        val preferences: SharedPreferences =
            getApplication<Application>().applicationContext.getSharedPreferences(
                "preferences",
                AppCompatActivity.MODE_PRIVATE
            )
        val versionDB = preferences.getString("versionDB", "")
        systemVariablesRepository.getValue("versionDB").addOnSuccessListener {
            if (!it!!.equals(versionDB)) {
                val realTeamsTask =
                    realTeamRepository.getAllRealTeams().addOnSuccessListener { realTeams ->
                        localDB.insertOrUpdateRealTeams(realTeams)
                    }.addOnFailureListener { e ->
                        Log.e(ContentValues.TAG, "error: get all teams from Firestore", e)
                    }
                Tasks.whenAll(realTeamsTask).addOnSuccessListener { _ ->
                    val editor: SharedPreferences.Editor = preferences.edit()
                    editor.putString("versionDB", it)
                    editor.apply()
                    isDBUpdated = true
                }
            }
        }.addOnFailureListener {
            Log.e(ContentValues.TAG, "error: get system variable versionDB from Firestore")
        }
        val currentMatchDay = preferences.getString("currentMatchDay", "")
        systemVariablesRepository.getValue("currentMatchDay").addOnSuccessListener {
            if (!it!!.equals(currentMatchDay) || isDBUpdated) {
                val playersTask = playerRepository.getAllPlayers().addOnSuccessListener { players ->
                    localDB.insertOrUpdatePlayers(players)
                }.addOnFailureListener { e ->
                    Log.e(ContentValues.TAG, "error: get all players from Firestore", e)
                }
                //Obtiene el matchday actual y el anterior, y una lista de tasks con cada partido
                // de la jornada en cada una
                val matchesTasks = mutableListOf<Task<Match>>()
                val matchDayTask =
                    matchDayRepository.getMatchDay(it).addOnSuccessListener { matchDay ->
                        matchesTasks.addAll(matchRepository.getMatchesByMatchDay(matchDay))
                    }.addOnFailureListener { e ->
                        Log.e(ContentValues.TAG, "error: get matchday from Firestore", e)
                    }
                val previousMatchDayTask =
                    matchDayRepository.getMatchDay((it.toInt() - 1).toString())
                        .addOnSuccessListener { previousMatchDay ->
                            matchesTasks.addAll(
                                matchRepository.getMatchesByMatchDay(
                                    previousMatchDay
                                )
                            )
                        }.addOnFailureListener { e ->
                            Log.e(ContentValues.TAG, "error: get matchday from Firestore", e)
                        }
                //Primero comprueba si se han terminado las tareas de jugadores y jornada, luego
                //compruebas la lista de tareas de partidos ya que sin la de jornada, la lista
                //estaría vacía
                Tasks.whenAll(playersTask, matchDayTask, previousMatchDayTask)
                    .addOnSuccessListener { _ ->
                        Tasks.whenAll(matchesTasks).addOnSuccessListener { _ ->
                            val matches = mutableListOf<Match>()
                            for (task in matchesTasks) {
                                matches.add(task.result!!)
                            }
                            localDB.insertOrUpdateMatches(matches)
                            val editor: SharedPreferences.Editor = preferences.edit()
                            editor.putString("currentMatchDay", it)
                            editor.apply()
                        }
                    }
            }
        }.addOnFailureListener {
            Log.e(ContentValues.TAG, "error: get system variable currentMatchDay from Firestore")
        }
    }


    fun getUserProfileFromAccount() {
        userProfileRepository.getByAccount(userAccountLiveData.value!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                userProfileLiveData.value = it.result
            }
        }
    }


    //###########################################
    // Lista de partidas
    //###########################################
    fun populateGameList(refresh: Boolean) {
        if (refresh || gameListLiveData.value.isNullOrEmpty()) {
            gameRepository.getGamesByUser(userProfileLiveData.value!!.id).addOnSuccessListener {
                Log.i(ContentValues.TAG, "success: game list obtained")
                gameListLiveData.value = it
            }.addOnFailureListener {
                try {
                    throw it
                } catch (t: Throwable) {
                    Log.e(ContentValues.TAG, t.message, it)
                }
            }
        }
    }

    //###########################################
    // Crear partidas
    //###########################################
    fun populateLeaguesList() {
        if (leaguesListLiveData.value.isNullOrEmpty()) {
            leagueRepository.getAll().addOnSuccessListener {
                leaguesListLiveData.value = it
            }.addOnFailureListener {
                Log.e(ContentValues.TAG, "error: get Leagues", it)
            }
        }
    }


    fun createGame(name: String, password: String, leagueId: String, invalidNameLocal: Boolean) {
        if (!correctGameFields(name, password, invalidNameLocal)) {
            successGameCreated.value = false
            return
        }
        val userReference = userProfileRepository.createReference(userProfileLiveData.value!!.id)
        val userTeam = UserTeam(
            credit = 15000000.00,
            userProfile = userReference,
            playersReserves = playerRepository.idListToPlayerRefList(
                localDB.selectRandomPlayersUserTeam(
                    null, true
                )
            )
        )
        val game = Game(
            "",
            name,
            password,
            Timestamp(Date().time),
            leagueRepository.createReference(leagueId),
            userReference,
            mutableListOf(userReference)
        )
        userTeamRepository.create(userTeam).addOnSuccessListener {
            game.userTeams.add(it)
            gameRepository.create(game).addOnSuccessListener { _ ->
                Log.i(ContentValues.TAG, "success: game and userTeam created")
                successGameCreated.value = true
            }.addOnFailureListener { e ->
                userTeamRepository.delete(it.id)
                successGameCreated.value = false
                Log.e(ContentValues.TAG, e.message, e)
            }
        }.addOnFailureListener {
            successGameCreated.value = false
            Log.e(ContentValues.TAG, it.message, it)
        }
    }

    //###########################################
    // Buscar partidas
    //###########################################
    fun searchGames(key: String) {
        if (!correctFieldsGameSearch(key)) {
            successSearchGame.value = false
            return
        }
        gameRepository.searchGames(key).addOnSuccessListener {
            Log.i(ContentValues.TAG, "success: game search success")
            successSearchGame.value = true
            val aux = mutableListOf<Game>()
            //Quitar las partidas en las que ya esta
            for (game in it) {
                if (!game.users.contains(userProfileRepository.createReference(userProfileLiveData.value!!.id))) {
                    aux.add(game)
                }
            }
            gamesFoundLiveData.value = aux
        }.addOnFailureListener {
            successSearchGame.value = false
            try {
                throw it
            } catch (t: Throwable) {
                Log.e(ContentValues.TAG, t.message, it)
            }
        }
    }

    fun joinGame(game: Game?, password: String) {
        if (!correctJoinGameFields(game, password)) {
            successJoinGame.value = false
            return
        }
        val userReference = userProfileRepository.createReference(userProfileLiveData.value!!.id)
        // Para comprobar que no se repitan jugadores, primero trae de firebase todos los equipos de la partida
        gameRepository.getTakenPlayersIdsByGame(game!!).addOnSuccessListener { playerDocRefs ->
            val playerTakenIDs = Utils.docRefListToIdList(playerDocRefs)
            val userTeam = UserTeam(
                credit = 10000000.00,
                userProfile = userReference,
                playersReserves = playerRepository.idListToPlayerRefList(
                    localDB.selectRandomPlayersUserTeam(
                        playerTakenIDs, true
                    )
                )
            )
            userTeamRepository.create(userTeam).addOnSuccessListener { userTeamRef ->
                gameRepository.joinGame(game.id, userReference, userTeamRef).addOnSuccessListener {
                    Log.i(ContentValues.TAG, "success: success join in game")
                    successJoinGame.value = true
                }.addOnFailureListener {
                    successJoinGame.value = false
                    Log.e(ContentValues.TAG, it.message, it)
                }
            }.addOnFailureListener {
                successJoinGame.value = false
                Log.e(ContentValues.TAG, it.message, it)
            }
        }.addOnFailureListener {
            successJoinGame.value = false
            Log.e(ContentValues.TAG, it.message, it)
        }

    }

    //###########################################
    // Métodos auxiliares
    //###########################################

    private fun correctGameFields(
        name: String,
        password: String,
        invalidNameLocal: Boolean
    ): Boolean {
        val context = getApplication<Application>().applicationContext
        if (name.isEmpty() || password.isEmpty()) {
            toastLiveData.value = context.resources.getString(R.string.empty_fields)
        } else if (name.length > context.resources.getInteger(R.integer.max_length_game_name)) {
            toastLiveData.value =
                context.resources.getString(R.string.long_game_name_1) + " " +
                        context.resources.getInteger(R.integer.max_length_game_name) + " " +
                        context.resources.getString(R.string.characters)
        } else if (password.length < context.resources.getInteger(R.integer.min_length_password)) {
            toastLiveData.value = context.resources.getString(R.string.invalid_password)
        } else if (invalidNameLocal) {
            toastLiveData.value = context.resources.getString(R.string.repeated_game_name)
        } else {
            return true
        }
        return false
    }

    private fun correctFieldsGameSearch(key: String): Boolean {
        val context = getApplication<Application>().applicationContext
        when {
            key.isEmpty() -> {
                toastLiveData.value = context.resources.getString(R.string.search_empty)
            }
            key.length > context.resources.getInteger(R.integer.max_length_game_name) -> {
                toastLiveData.value =
                    context.resources.getString(R.string.long_game_name_1) + " " +
                            context.resources.getInteger(R.integer.max_length_game_name) + " " +
                            context.resources.getString(R.string.characters)
            }
            else -> {
                return true
            }
        }
        return false
    }

    private fun correctJoinGameFields(game: Game?, password: String): Boolean {
        val context = getApplication<Application>().applicationContext
        when {
            game == null -> {
                toastLiveData.value = context.resources.getString(R.string.no_game_selected)
            }
            game.users.size >= context.resources.getInteger(R.integer.max_users_game) -> {
                toastLiveData.value = context.resources.getString(R.string.empty_password)
            }
            password.isEmpty() -> {
                toastLiveData.value = context.resources.getString(R.string.empty_password)
            }
            password != game.password -> {
                wrongPasswordLiveData.value = true
                toastLiveData.value = context.resources.getString(R.string.wrong_password)
            }
            else -> {
                wrongPasswordLiveData.value = false
                return true
            }
        }
        return false
    }

    @Suppress("ReplaceCallWithBinaryOperator")
    fun checkGameNameAux(gameName: String) {
        if (gameName.isBlank())
            return
        gameRepository.getByName(gameName).addOnCompleteListener {
            invalidGameNameLiveData.value = it.isSuccessful
            if (!it.isSuccessful) {
                Log.e("error", it.exception!!.message!!, it.exception)
            }
        }
    }
}