package com.chema.fichajesdeportivos.viewModel

import android.app.Application
import android.content.ContentValues
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.UserProfile
import android.os.Handler
import android.os.Looper
import com.chema.fichajesdeportivos.repository.GameRepository
import com.chema.fichajesdeportivos.repository.UserProfileRepository
import com.chema.fichajesdeportivos.repository.UserTeamRepository
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.*


class AccountSettingsViewModel(
    application: Application,
    userAccount: FirebaseUser,
    val userProfile: UserProfile
) : AndroidViewModel(application) {

    private val userProfileRepository = UserProfileRepository.instance
    private val gameRepository = GameRepository.instance
    private val userTeamRepository = UserTeamRepository.instance

    val userAccountLiveData = MutableLiveData(userAccount)
    val messageDeleteLiveData = MutableLiveData<String>()

    // 0: Estado inicial; 1: Ok; 2: Contraseña incorrecta, 3: Otro error
    var successfulDeleteLiveData = MutableLiveData<Byte>(0)

    fun deleteUser(password: String) {
        if (password.isNullOrEmpty()) {
            successfulDeleteLiveData.value = 2
            return
        }
        val userAccount = userAccountLiveData.value
        val context = getApplication<Application>().applicationContext
        val credential = EmailAuthProvider.getCredential(userAccount?.email!!, password)

        userAccount.reauthenticate(credential).addOnSuccessListener { _ ->
            userAccount.delete().addOnSuccessListener { _ ->
                Log.i(ContentValues.TAG, context.resources.getString(R.string.delete_account_ok))
                deleteUserData(context)
            }.addOnFailureListener {
                try {
                    throw it
                } catch (t: FirebaseAuthRecentLoginRequiredException) {
                    //Segundo intento
                    Handler(Looper.getMainLooper()).postDelayed({
                        userAccount.delete().addOnSuccessListener { _ ->
                            Log.i(
                                ContentValues.TAG, "Second try: " +
                                        context.resources.getString(R.string.delete_account_ok)
                            )
                            deleteUserData(context)
                        }.addOnFailureListener { e ->
                            Log.e(ContentValues.TAG, "Second try: " + t.message!!, e)
                            unknownError(context)
                        }
                    }, 2000)
                } catch (t: Throwable) {
                    Log.e(ContentValues.TAG, t.message!!, it)
                    unknownError(context)
                }
            }
        }.addOnFailureListener {
            try {
                throw it
            } catch (e: FirebaseAuthInvalidCredentialsException) {
                if (e.errorCode.equals("ERROR_WRONG_PASSWORD")) {
                    Log.e(ContentValues.TAG, e.message!!, e)
                    successfulDeleteLiveData.value = 0
                    successfulDeleteLiveData.value = 2
                    messageDeleteLiveData.value =
                        context.resources.getString(R.string.wrong_password)
                } else {
                    Log.e(ContentValues.TAG, e.message!!)
                }
            } catch (t: Throwable) {
                Log.e(ContentValues.TAG, t.message!!)
                unknownError(context)
            }
        }
    }

    private fun unknownError(context: Context) {
        successfulDeleteLiveData.value = 0
        successfulDeleteLiveData.value = 3
        messageDeleteLiveData.value = context.resources.getString(R.string.unknown_error)
    }

    private fun deleteUserData(context: Context) {
        val gamesTask = gameRepository.getGamesByUser(userProfile.id)
        val userTeamsTask =
            userTeamRepository.getUserTeamsIdsByUser(
                userProfileRepository.createReference(
                    userProfile.id
                )
            )
        Tasks.whenAll(gamesTask, userTeamsTask).addOnSuccessListener {
            userProfileRepository.deleteUserAndAllData(
                userProfileRepository.createReference(userProfile.id),
                gamesTask.result!!,
                userTeamsTask.result!!
            ).addOnSuccessListener {
                Log.i(ContentValues.TAG, "Account Data removed successfully")
                FirebaseAuth.getInstance().signOut()
                successfulDeleteLiveData.value = 1
            }.addOnFailureListener {
                Log.e(ContentValues.TAG, "Delete account OK but fail to delete data account")
                Log.e(ContentValues.TAG, it.message!!, it)
                unknownError(context)
            }
        }.addOnFailureListener {
            Log.e(ContentValues.TAG, "Delete account OK but fail to delete data account")
            Log.e(ContentValues.TAG, it.message!!, it)
            unknownError(context)
        }
    }

}