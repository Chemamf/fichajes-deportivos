package com.chema.fichajesdeportivos.viewModel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.chema.fichajesdeportivos.R
import android.content.ContentValues.TAG
import android.os.Handler
import android.os.Looper
import com.google.firebase.auth.*
import kotlinx.coroutines.delay


class ChangePasswordViewModel(
    application: Application,
    private var userAccount: FirebaseUser,
    username: String
) : AndroidViewModel(application) {

    val userProfileLiveData = MutableLiveData(username)
    val messageLiveData = MutableLiveData<String>()
    var successfulLiveData = MutableLiveData(false)

    fun updatePassword(
        oldPassword: String,
        newPassword: String,
        newPasswordRepeated: String
    ) {
        if (!checkFields(oldPassword, newPassword, newPasswordRepeated)) {
            successfulLiveData.value = false
            return
        }
        val context = getApplication<Application>().applicationContext
        val credential = EmailAuthProvider.getCredential(userAccount.email!!, oldPassword)

        userAccount.reauthenticate(credential).onSuccessTask {
            userAccount.updatePassword(newPassword).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.i(TAG, context.resources.getString(R.string.update_password_ok))
                    messageLiveData.value =
                        context.resources.getString(R.string.update_password_ok)
                    successfulLiveData.value = true
                    FirebaseAuth.getInstance().signOut()
                } else {
                    try {
                        throw task.exception!!
                    } catch (t: FirebaseAuthRecentLoginRequiredException) {
                        //Segundo intento
                        Handler(Looper.getMainLooper()).postDelayed({
                            userAccount.updatePassword(newPassword).addOnCompleteListener { secondTask ->
                                if (secondTask.isSuccessful) {
                                    Log.i(
                                        TAG,
                                        context.resources.getString(R.string.update_password_ok)
                                    )
                                    messageLiveData.value =
                                        context.resources.getString(R.string.update_password_ok)
                                    successfulLiveData.value = true
                                    FirebaseAuth.getInstance().signOut()
                                } else {
                                    try {
                                        throw secondTask.exception!!
                                    } catch (t: Throwable) {
                                        Log.e(TAG, t.message!!, secondTask.exception)
                                        successfulLiveData.value = false
                                        messageLiveData.value =
                                            context.resources.getString(R.string.unknown_error)
                                    }
                                }
                            }
                        }, 1000)
                    } catch (t: Throwable) {
                        Log.e(TAG, t.message!!)
                        successfulLiveData.value = false
                        messageLiveData.value =
                            context.resources.getString(R.string.unknown_error)
                    }
                }
            }
        }.addOnFailureListener {
            successfulLiveData.value = false
            try {
                throw it
            } catch (e: FirebaseAuthInvalidCredentialsException) {
                if (e.errorCode.equals("ERROR_WRONG_PASSWORD")) {
                    Log.d(TAG, e.message!!)
                    messageLiveData.value =
                        context.resources.getString(R.string.wrong_old_password)
                } else {
                    Log.d(TAG, e.message!!)
                    messageLiveData.value =
                        context.resources.getString(R.string.unknown_error)
                }
            } catch (t: Throwable) {
                Log.d(TAG, t.message!!)
                messageLiveData.value = context.resources.getString(R.string.unknown_error)
            }
        }

    }

    private fun checkFields(
        oldPassword: String,
        newPassword: String,
        newPasswordRepeated: String
    ): Boolean {
        val context = getApplication<Application>().applicationContext
        var correctFields = false
        if (oldPassword.isEmpty() || newPassword.isEmpty() || newPasswordRepeated.isEmpty()) {
            messageLiveData.value = context.resources.getString(R.string.empty_fields)
        } else if (newPassword.length < context.resources.getInteger(R.integer.min_length_password)) {
            messageLiveData.value = context.resources.getString(R.string.invalid_password)
        } else if (!newPassword.equals(newPasswordRepeated)) {
            messageLiveData.value = context.resources.getString(R.string.nonequal_passwords)
        } else {
            correctFields = true
        }
        return correctFields
    }


}