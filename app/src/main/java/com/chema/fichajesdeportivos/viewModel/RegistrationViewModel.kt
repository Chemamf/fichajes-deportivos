package com.chema.fichajesdeportivos.viewModel

import android.app.Application
import android.content.ContentValues
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.UserProfile
import com.chema.fichajesdeportivos.repository.UserProfileRepository
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseUser

class RegistrationViewModel(application: Application) : AndroidViewModel(application) {

    private var auth: FirebaseAuth = FirebaseAuth.getInstance()
    private val userProfileRepository = UserProfileRepository.instance

    val userAccountLiveData = MutableLiveData<FirebaseUser>()

    val invalidUsernameLiveData = MutableLiveData(true)
    val invalidEmailLiveData = MutableLiveData<String>("")
    val successLiveData = MutableLiveData<Boolean>()
    val toastLiveData = MutableLiveData<String>()


    fun registration(username: String, email: String, password: String, repeatPassword: String, invalidUsernameLocal: Boolean) {
        val context = getApplication<Application>().applicationContext
        if (!correctFields(username, email, password, repeatPassword,invalidUsernameLocal)){
            successLiveData.value = false
            return
        }
        userProfileRepository.create(UserProfile(username = username)).addOnCompleteListener {
            if (it.isSuccessful) {
                Log.d(ContentValues.TAG, "createUsername:success")
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener() { task ->
                        if (task.isSuccessful) {
                            Log.d(ContentValues.TAG, "createUserWithEmail:success")
                            userAccountLiveData.value = auth.currentUser!!
                            it.result!!.update("userAccountUID", userAccountLiveData.value!!.uid)
                            successLiveData.value = true
                        } else {
                            try {
                                throw task.exception!!
                            } catch (emailRepeated: FirebaseAuthUserCollisionException) {
                                Log.e(
                                    ContentValues.TAG,
                                    "createUserWithEmail:failure",
                                    task.exception
                                )
                                userProfileRepository.delete(it.result!!.id)
                                successLiveData.value = false
                                invalidEmailLiveData.value = email
                                toastLiveData.value =
                                    context.resources.getString(R.string.repeated_email)
                            } catch (t: Throwable) {
                                Log.e(
                                    ContentValues.TAG,
                                    "createUserWithEmail:failure",
                                    task.exception
                                )
                                userProfileRepository.delete(it.result!!.id)
                                successLiveData.value = false
                                toastLiveData.value =
                                    context.resources.getString(R.string.signUp_failed)
                            }
                        }
                    }
            } else {
                Log.w(ContentValues.TAG, "createUserWithEmail:failure", it.exception)
                successLiveData.value = false
                toastLiveData.value = context.resources.getString(R.string.signUp_failed)
            }
        }
    }

    private fun correctFields(
        username: String,
        email: String,
        password: String,
        repeatPassword: String,
        invalidUsernameLocal : Boolean
    ): Boolean {
        val context = getApplication<Application>().applicationContext
        var correctFields = false
        if (username.isEmpty() || email.isEmpty() || password.isEmpty() || repeatPassword.isEmpty()) {
            toastLiveData.value = context.resources.getString(R.string.empty_fields)
        } else if (username.length > context.resources.getInteger(R.integer.max_length_username)) {
            toastLiveData.value = context.resources.getString(R.string.long_username_1) + " " +
                    context.resources.getInteger(R.integer.max_length_username) + " " +
                    context.resources.getString(R.string.characters)
        } else if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            toastLiveData.value = context.resources.getString(R.string.invalid_email)
        } else if (password.length < context.resources.getInteger(R.integer.min_length_password)) {
            toastLiveData.value = context.resources.getString(R.string.invalid_password)
        } else if (!password.equals(repeatPassword)) {
            toastLiveData.value = context.resources.getString(R.string.nonequal_passwords)
        } else if (invalidUsernameLiveData.value!! || invalidUsernameLocal) {
            toastLiveData.value = context.resources.getString(R.string.repeated_username)
        } else if (email == invalidEmailLiveData.value!!) {
            toastLiveData.value = context.resources.getString(R.string.repeated_email)
        } else {
            correctFields = true
        }
        return correctFields
    }

    @Suppress("ReplaceCallWithBinaryOperator")
    fun checkUsernameAux(username: String) {
        if (username.isBlank())
            return
        userProfileRepository.getByUsername(username)
            .addOnCompleteListener {
                invalidUsernameLiveData.value = it.isSuccessful
            }

    }
}