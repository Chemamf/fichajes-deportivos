package com.chema.fichajesdeportivos.viewModel.factory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.chema.fichajesdeportivos.viewModel.ChangePasswordViewModel
import com.google.firebase.auth.FirebaseUser

class ChangePasswordViewModelFactory(
    private val application: Application,
    private val userAccount: FirebaseUser,
    private val username: String
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ChangePasswordViewModel(application, userAccount, username) as T
    }
}