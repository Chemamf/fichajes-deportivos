package com.chema.fichajesdeportivos.viewModel.factory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.chema.fichajesdeportivos.viewModel.MainViewModel
import com.google.firebase.auth.FirebaseUser


class MainViewModelFactory(private val application: Application,  private val user: FirebaseUser) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(application, user) as T
    }

}