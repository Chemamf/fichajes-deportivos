package com.chema.fichajesdeportivos.viewModel.factory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.chema.fichajesdeportivos.model.UserProfile
import com.chema.fichajesdeportivos.viewModel.GameViewModel


class GameViewModelFactory(
    private val application: Application,
    private val userProfile: UserProfile
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return GameViewModel(application, userProfile) as T
    }
}