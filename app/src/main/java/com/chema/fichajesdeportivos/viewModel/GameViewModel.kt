package com.chema.fichajesdeportivos.viewModel

import android.app.Application
import android.content.ContentValues
import android.content.SharedPreferences
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.*
import com.chema.fichajesdeportivos.model.enum.Formation
import com.chema.fichajesdeportivos.model.enum.Position
import com.chema.fichajesdeportivos.other.Utils
import com.chema.fichajesdeportivos.repository.*
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.auth.User
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import java.util.function.LongFunction

class GameViewModel(application: Application, userProfile: UserProfile) :
    AndroidViewModel(application) {

    //Instancias de repositorios
    private val gameRepository = GameRepository.instance
    private val localDB = DB(getApplication<Application>().applicationContext)
    private val matchDayRepository = MatchDayRepository.instance
    private val userTeamRepository = UserTeamRepository.instance
    private val userProfileRepository = UserProfileRepository.instance
    private val playerRepository = PlayerRepository.instance
    private val offerRepository = OfferRepository.instance

    //LiveDatas
    val gameLiveData = MutableLiveData<Game>()
    val userLiveData = MutableLiveData(userProfile)
    val usersLiveData = MutableLiveData<List<UserProfile>>()
    val userTeamLiveData = MutableLiveData<UserTeam>()
    val userTeamsLiveData = MutableLiveData<List<UserTeam>>()
    val gameDataRetrieveLiveData = MutableLiveData(false)
    val leagueLiveData = MutableLiveData<League>()
    val toastLiveData = MutableLiveData<String>()

    //Jornadas
    val currentMatchdayLiveData = MutableLiveData<MatchDay>()
    val lastMatchdayLiveData = MutableLiveData<MatchDay>()
    val lastMatchesLiveData = MutableLiveData<List<Match>>()
    val matchdaysDataRetrieveLiveData = MutableLiveData(false)

    //Partidos
    val currentMatchSelectedLiveData = MutableLiveData<Match>()

    //Información de partida
    val gameInfoUpdatedLiveData = MutableLiveData(false)
    val userToDeleteFromGamePositionLiveData = MutableLiveData<Int>()

    //Mi equipo
    val onEditUserTeamLiveData =
        MutableLiveData<Triple<Formation, Map<Position, MutableList<Player>>, Map<Position, MutableList<Player>>>>()
    val positionUpdatedLiveData = MutableLiveData(Position.DEFAULT)
    val formationUpdatedLiveData = MutableLiveData(false)

    //Ofertas
    val marketLiveData = MutableLiveData<MutableList<Offer>>()
    val marketRetrievedLiveData = MutableLiveData(false)

    //Primer valor del par: True para comprar, false para vender
    val offerPlayerLiveData = MutableLiveData<Triple<Boolean, Player, Offer>>()

    /**0: Estado inicial, 1: Oferta de compra hecha, 2:oferta de compra eliminada, 3: Oferta de venta creada,
    4: Oferta de venta eliminada, 5: Oferta aceptada, 6: Oferta rechazada * Errores: Igual empezando por 1*/
    val offerActionSuccessLiveData = MutableLiveData<Byte>(0)


    fun getGameAndData(gameId: String) {
        gameRepository.getGameAndData(gameId).addOnSuccessListener {
            gameLiveData.value = it["game"]!![0].toObject(Game::class.java)
            leagueLiveData.value = it["league"]!![0].toObject(League::class.java)
            val usersAux = mutableListOf<UserProfile>()
            val userTeamsAux = mutableListOf<UserTeam>()
            // Recorre la lista de snapshots de userTeam ordenandola antes por puntos, lo añade a la lista de
            // UserTeam convertido en UserTeam, y busca el primer User cuyo id del snapshot coincida
            // con el id del ultimo Team de la lista de Teams (el que se acaba de añadir), lo convierte en
            // UserProfile y lo a añade a la lista de users
            for (utss in (it["userTeams"] as List<DocumentSnapshot>).sortedBy { ss -> ss["totalPoints"] as Long }
                .reversed()) {
                val ut = utss.toObject(UserTeam::class.java)!!
                if (ut.userProfile!!.id.equals(userLiveData.value!!.id))
                    userTeamLiveData.value = ut
                userTeamsAux.add(ut)
                usersAux.add(it["users"]!!.first { u -> u.id.equals(userTeamsAux[userTeamsAux.size - 1].userProfile?.id) }
                    .toObject(UserProfile::class.java)!!)
            }
            usersLiveData.value = usersAux
            userTeamsLiveData.value = userTeamsAux
            //Actualizar parametros para observers
            if (!gameDataRetrieveLiveData.value!!) {
                gameDataRetrieveLiveData.value = true
            }
            gameInfoUpdatedLiveData.value = true
        }.addOnFailureListener {
            Log.e(ContentValues.TAG, "error: get Game and Data Firestore: " + it.message, it)
        }
    }

    //###########################################
    // Clasificación
    //###########################################

    fun getTeamValue(userTeam: UserTeam): String {
        var sum: Long = 0
        val userPlayers = mutableListOf<DocumentReference>()
        userPlayers.addAll(userTeam.playersLineUp)
        userPlayers.addAll(userTeam.playersReserves)
        val players = localDB.selectMultiplePlayersByIds(Utils.docRefListToIdList(userPlayers))
        for (p in players) {
            sum += p.value
        }
        return Utils.valueFormat(sum)
    }

    //###########################################
    // Última jornada
    //###########################################

    fun getMatchdaysAndMatches() {
        matchDayRepository.getCurrentAndLastMatchDay().addOnSuccessListener {
            lastMatchdayLiveData.value = it.first
            currentMatchdayLiveData.value = it.second
            lastMatchesLiveData.value =
                localDB.selectMatchesByMatchday(Utils.docRefListToIdList(it.first.matches))
            matchdaysDataRetrieveLiveData.value = true
        }.addOnFailureListener {
            Log.e(ContentValues.TAG, "error: get current and last matchday:" + it.message, it)
        }
    }

    fun getTeams(): List<RealTeam> {
        return localDB.selectAllRealTeams()
    }


    //###########################################
    // Partidos
    //###########################################

    /**Devuelve una lista con dos Triples, cada uno con equipo, listas de Jugadores, locales y visitantes respectivamente*/
    fun getPlayersMatch(): List<Triple<RealTeam, List<Player>, Int>> {
        val realTeams =
            localDB.selectRealTeamsByIdList(
                listOf(
                    currentMatchSelectedLiveData.value!!.homeTeam!!.id,
                    currentMatchSelectedLiveData.value!!.awayTeam!!.id
                )
            )
        val playersHome =
            localDB.selectMultiplePlayersByIds(Utils.docRefListToIdList(realTeams[0].players))
        val playersAway =
            localDB.selectMultiplePlayersByIds(Utils.docRefListToIdList(realTeams[1].players))
        return listOf(
            Triple(
                realTeams[0],
                playersHome,
                getTeamPoints(playersHome)
            ),
            Triple(
                realTeams[1],
                playersAway,
                getTeamPoints(playersAway)
            )
        )
    }

    fun getTeamPoints(players: List<Player>): Int {
        var result = 0
        for (p in players)
            result += p.pointsLastMatch
        return result
    }

    //###########################################
    // Información de partida
    //###########################################

    fun deleteUser(position: Int) {
        gameInfoUpdatedLiveData.value = false
        gameRepository.removeUserFromGame(
            gameLiveData.value!!.id,
            usersLiveData.value!![position].id,
            userTeamsLiveData.value!![position].id
        ).addOnSuccessListener {
            getGameAndData(gameLiveData.value!!.id)
        }.addOnFailureListener {
            Log.e(ContentValues.TAG, it.message, it)
        }
    }

    //###########################################
    // Información de partida
    //###########################################

    fun getNumberPlayersCurrent(): Int {
        val lu = userTeamLiveData.value!!.playersLineUp.size
        val r = userTeamLiveData.value!!.playersReserves.size
        return r + lu
    }

    //###########################################
    // Mi equipo
    //###########################################

    fun setSavedPlayersMyTeam() {
        onEditUserTeamLiveData.value = Triple(
            userTeamLiveData.value!!.formation,
            filterPlayersByPosition(true),
            filterPlayersByPosition(false)
        )
    }

    private fun filterPlayersByPosition(isLineUp: Boolean): Map<Position, MutableList<Player>> {
        val result = mutableMapOf(
            Position.GOALKEEPER to mutableListOf<Player>(),
            Position.DEFENDER to mutableListOf(),
            Position.MIDFIELDER to mutableListOf(),
            Position.ATTACKER to mutableListOf(),
            Position.DEFAULT to mutableListOf()
        )
        val players = if (isLineUp) {
            localDB.selectMultiplePlayersByIds(Utils.docRefListToIdList(userTeamLiveData.value!!.playersLineUp)) as MutableList<Player>
        } else {
            localDB.selectMultiplePlayersByIds(Utils.docRefListToIdList(userTeamLiveData.value!!.playersReserves)) as MutableList<Player>
        }
        for (p in players) {
            when (p.position) {
                Position.GOALKEEPER -> result[Position.GOALKEEPER]!!.add(p)
                Position.DEFENDER -> result[Position.DEFENDER]!!.add(p)
                Position.MIDFIELDER -> result[Position.MIDFIELDER]!!.add(p)
                Position.ATTACKER -> result[Position.ATTACKER]!!.add(p)
                else -> result[Position.DEFAULT]!!.add(p)
            }
        }
        return result
    }

    fun changePlayer(player: Player, addLineUp: Boolean) {
        if (addLineUp) {
            onEditUserTeamLiveData.value!!.third[player.position]!!.remove(player)
            onEditUserTeamLiveData.value!!.second[player.position]!!.add(player)
        } else {
            onEditUserTeamLiveData.value!!.second[player.position]!!.remove(player)
            onEditUserTeamLiveData.value!!.third[player.position]!!.add(player)
        }
        positionUpdatedLiveData.value = player.position
    }

    fun changeFormation(formation: Formation) {
        onEditUserTeamLiveData.value = onEditUserTeamLiveData.value!!.copy(first = formation)
        while (onEditUserTeamLiveData.value!!.second[Position.DEFENDER]!!.size > formation.defence) {
            val player = onEditUserTeamLiveData.value!!.second[Position.DEFENDER]!!.removeLast()
            onEditUserTeamLiveData.value!!.third[Position.DEFENDER]!!.add(player)
        }
        while (onEditUserTeamLiveData.value!!.second[Position.MIDFIELDER]!!.size > formation.midfield) {
            val player = onEditUserTeamLiveData.value!!.second[Position.MIDFIELDER]!!.removeLast()
            onEditUserTeamLiveData.value!!.third[Position.MIDFIELDER]!!.add(player)
        }
        while (onEditUserTeamLiveData.value!!.second[Position.ATTACKER]!!.size > formation.forward) {
            val player = onEditUserTeamLiveData.value!!.second[Position.ATTACKER]!!.removeLast()
            onEditUserTeamLiveData.value!!.third[Position.ATTACKER]!!.add(player)
        }
        formationUpdatedLiveData.value = true
    }

    fun saveUserTeam() {
        val lineUp = mutableListOf<Player>()
        val reserves = mutableListOf<Player>()
        for (v in onEditUserTeamLiveData.value!!.second.values) {
            lineUp.addAll(v)
        }
        for (v in onEditUserTeamLiveData.value!!.third.values) {
            reserves.addAll(v)
        }
        userTeamLiveData.value!!.formation = onEditUserTeamLiveData.value!!.first
        userTeamLiveData.value!!.playersLineUp = Utils.objectListToDocRefList(
            lineUp,
            playerRepository.getCollectionReference()
        ) as MutableList
        userTeamLiveData.value!!.playersReserves = Utils.objectListToDocRefList(
            reserves,
            playerRepository.getCollectionReference()
        ) as MutableList

        userTeamRepository.updateLineUpAndFormation(userTeamLiveData.value!!).addOnFailureListener {
            Log.e(ContentValues.TAG, it.message, it)
        }
    }

    fun getAllPlayers(): Pair<List<Player>, List<Player>> {
        val players = mutableListOf<Player>()
        players.addAll(localDB.selectMultiplePlayersByIds(Utils.docRefListToIdList(userTeamLiveData.value!!.playersLineUp)) as MutableList<Player>)
        players.addAll(localDB.selectMultiplePlayersByIds(Utils.docRefListToIdList(userTeamLiveData.value!!.playersReserves)) as MutableList<Player>)
        players.sortBy { it.value }
        val list1 = mutableListOf<Player>()
        val list2 = mutableListOf<Player>()
        for (i in 0 until players.size) {
            if (i == 0 || i % 2 == 0) {
                list1.add(players[i])
            } else {
                list2.add(players[i])
            }
        }
        return Pair(list1, list2)
    }

    //###########################################
    // Mercado
    //###########################################

    fun updateMarket() {
        if (gameLiveData.value!!.market.isNullOrEmpty()) {
            if (Date().time < currentMatchdayLiveData.value!!.startMatchPeriod!!.time) {
                createMarket().addOnSuccessListener {
                    marketLiveData.value = it
                    marketRetrievedLiveData.value = true
                }
            }
        } else {
            offerRepository.getOffersByGame(gameLiveData.value!!).addOnSuccessListener {
                marketLiveData.value = it
                if (it[0].matchDay.equals(currentMatchdayLiveData.value!!.id)) {
                    if (Date().time > currentMatchdayLiveData.value!!.startMatchPeriod!!.time) {
                        val task = removeOrResolveOffers(false)
                        task.first.addOnSuccessListener {
                            marketLiveData.value = mutableListOf()
                            marketRetrievedLiveData.value = true
                        }
                    } else if (Date().time.minus(86400000L) > currentMatchdayLiveData.value!!.startMatchPeriod!!.time) {
                        val task = removeOrResolveOffers(true)
                        task.first.addOnSuccessListener {
                            marketLiveData.value!!.removeAll(task.second)
                            marketRetrievedLiveData.value = true
                        }
                    } else {
                        marketLiveData.value = it
                        marketRetrievedLiveData.value = true
                    }
                } else {
                    val task = removeOrResolveOffers(false)
                    task.first.addOnSuccessListener {
                        createMarket().addOnSuccessListener { newMarket ->
                            marketLiveData.value = newMarket
                            marketRetrievedLiveData.value = true
                        }
                    }
                }
            }.addOnFailureListener {
                Log.e(ContentValues.TAG, it.message, it)
            }
        }
    }

    private fun createMarket(): Task<MutableList<Offer>> {
        val takenPlayers = mutableListOf<DocumentReference>()
        for (ut in userTeamsLiveData.value!!) {
            takenPlayers.addAll(ut.playersLineUp)
            takenPlayers.addAll(ut.playersReserves)
        }
        val marketPlayers =
            localDB.selectMultiplePlayersByIds(
                localDB.selectRandomPlayersUserTeam(
                    Utils.docRefListToIdList(
                        takenPlayers
                    ), false
                )
            )
        val market = mutableListOf<Offer>()
        for (p in marketPlayers) {
            market.add(
                Offer(
                    price = p.value.toDouble(),
                    seller = userProfileRepository.createReference("IA"),
                    matchDay = currentMatchdayLiveData.value!!.id,
                    player = playerRepository.createReference(p.id)
                )
            )
        }
        return offerRepository.createOffers(market, gameLiveData.value!!.id).addOnSuccessListener {
            gameLiveData.value!!.market = Utils.objectListToDocRefList(
                it,
                offerRepository.getCollectionReference()
            ) as MutableList<DocumentReference>
            Log.i(ContentValues.TAG, "create Offers: Ok")
        }.addOnFailureListener {
            Log.e(ContentValues.TAG, it.message, it)
        }
    }

    private fun removeOrResolveOffers(onlyResolve: Boolean): Pair<Task<Void>, MutableList<Offer>> {
        val iaOffers = mutableListOf<Offer>()
        for (o in marketLiveData.value!!) {
            if (o.seller!!.id.equals("IA")) {
                iaOffers.add(o)
            }
        }
        val userTeamsAux = resolveIAMarket(iaOffers)
        val toDelete = if (!onlyResolve) marketLiveData.value!! else iaOffers
        val task = offerRepository.resolveAndDeleteOffers(userTeamsAux, toDelete)
            .addOnSuccessListener {
                userTeamsLiveData.value = userTeamsAux
                userTeamLiveData.value =
                    userTeamsAux.first { ut -> ut.userProfile!!.id.equals(userLiveData.value!!.id) }
            }.addOnFailureListener {
                Log.e(ContentValues.TAG, it.message, it)
            }
        return Pair(task, iaOffers)
    }

    private fun resolveIAMarket(offers: List<Offer>): List<UserTeam> {
        val userTeamsAux = mutableListOf<UserTeam>()
        userTeamsAux.addAll(userTeamsLiveData.value!!)
        for (o in offers) {
            if (o.bidding.isNotEmpty()) {
                val bestOffer = o.bidding.maxByOrNull { v -> v.value }
                val buyer = userTeamsAux.firstOrNull { u -> u.userProfile!!.id == bestOffer!!.key }
                if (buyer != null) {
                    buyer.playersReserves.add(o.player!!)
                    buyer.credit -= bestOffer!!.value
                    userTeamsAux.add(buyer)
                }
            }
        }
        return userTeamsAux

    }

    fun getMarketLists(): Pair<List<MutableList<Offer>>, List<Player>> {
        val market1 = mutableListOf<Offer>()
        val market2 = mutableListOf<Offer>()
        for (i in 0 until marketLiveData.value!!.size) {
            if (i == 0 || i % 2 == 0) {
                market1.add(marketLiveData.value!![i])
            } else {
                market2.add(marketLiveData.value!![i])
            }
        }
        val players = getPlayersByOffers(marketLiveData.value!!)
        return Pair(listOf(market1, market2), players)
    }

    private fun getPlayersByOffers(market: List<Offer>): List<Player> {
        val aux = mutableListOf<DocumentReference>()
        for (o in market) {
            aux.add(o.player!!)
        }
        return localDB.selectMultiplePlayersByIds(Utils.docRefListToIdList(aux))
    }

    fun getAllRealTeams(): List<RealTeam> {
        return localDB.selectAllRealTeams()
    }

    fun offerToBuy(): Boolean {
        //Si la oferta es nula o el usuario no es una clave de binding, devuelve false
        return offerPlayerLiveData.value!!.third.bidding.containsKey(userLiveData.value!!.id)
            ?: false
    }

    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    fun doOffer(amount: String, amountSize: String) {
        val dAmount = Utils.valueAndSizeToDouble(amount, amountSize)
        if (dAmount == null) {
            offerActionSuccessLiveData.value = (11).toByte()
        }
        val offerAux = offerPlayerLiveData.value!!.third.copy()
        (offerAux.bidding as MutableMap<String, Double>)[userLiveData.value!!.id] = dAmount!!
        offerRepository.updateBiddingOffer(offerPlayerLiveData.value!!.third).addOnSuccessListener {
            offerActionSuccessLiveData.value = 1.toByte()
            offerPlayerLiveData.value = null
        }.addOnFailureListener {
            Log.e(ContentValues.TAG, it.message, it)
            offerActionSuccessLiveData.value = (11).toByte()
        }
    }

    fun deleteBid() {
        val offerAux = offerPlayerLiveData.value!!.third
        (offerAux.bidding as MutableMap<String, Double>).remove(userLiveData.value!!.id)
        offerRepository.updateBiddingOffer(offerAux).addOnSuccessListener {
            offerActionSuccessLiveData.value = 2.toByte()
            offerPlayerLiveData.value = null
        }.addOnFailureListener {
            Log.e(ContentValues.TAG, it.message, it)
            offerActionSuccessLiveData.value = (12).toByte()
        }
    }

    fun addOffer(player: Player?, price: String, priceSize: String) {
        if (!checkOfferFields(player, price)) {
            return
        }
        val dPrice = Utils.valueAndSizeToDouble(price, priceSize)
        if (dPrice == null) {
            offerActionSuccessLiveData.value = 13.toByte()
        }
        val offer = Offer(
            price = dPrice!!,
            seller = userProfileRepository.createReference(userLiveData.value!!.id),
            bidding = mutableMapOf("IA" to player!!.value.toDouble()),
            matchDay = currentMatchdayLiveData.value!!.id,
            player = playerRepository.createReference(player.id)
        )
        offerRepository.createOffer(offer, gameLiveData.value!!.id).addOnSuccessListener {
            Log.i(ContentValues.TAG, "addOffer: Success!")
            marketLiveData.value!!.add(it)
            gameLiveData.value!!.market.add(offerRepository.createReference(it.id))
            offerActionSuccessLiveData.value = 3.toByte()
            offerPlayerLiveData.value = null
        }.addOnFailureListener {
            Log.e(ContentValues.TAG, it.message, it)
            offerActionSuccessLiveData.value = 13.toByte()
        }
    }

    fun deleteOffer() {
        offerRepository.deleteOffer(offerPlayerLiveData.value!!.third.id, gameLiveData.value!!.id)
            .addOnSuccessListener {
                Log.i(ContentValues.TAG, "Delete offer: Success!")
                marketLiveData.value!!.remove(marketLiveData.value!!.first {
                    it.id.equals(
                        offerPlayerLiveData.value!!.third.id
                    )
                })
                gameLiveData.value!!.market.remove(gameLiveData.value!!.market.first {
                    it.id.equals(
                        offerPlayerLiveData.value!!.third.id
                    )
                })
                offerActionSuccessLiveData.value = 4.toByte()
                offerPlayerLiveData.value = null
            }.addOnFailureListener {
                Log.e(ContentValues.TAG, it.message, it)
                offerActionSuccessLiveData.value = 14.toByte()
            }
    }

    fun resolveOffer(values: Map.Entry<String, Double>, offer: Offer, isAccepted: Boolean) {
        if (isAccepted) {
            val buyerTeam =
                userTeamsLiveData.value!!.firstOrNull { it.userProfile!!.id.equals(values.key) }
            val whereIsPlayer =
                if (userTeamLiveData.value!!.playersLineUp.any { it.id.equals(offer.player!!.id) }) "playersLineUp"
                else "playersReserves"
            offerRepository.acceptOffer(
                buyerTeam,
                userTeamLiveData.value!!,
                offer,
                values.value,
                gameLiveData.value!!.id,
                whereIsPlayer
            ).addOnSuccessListener {
                Log.i(ContentValues.TAG, "Accept offer: Success!")
                if (whereIsPlayer.equals("playersLineUp")) {
                    userTeamLiveData.value!!.playersLineUp.remove(userTeamLiveData.value!!.playersLineUp.first {
                        it.id.equals(
                            offer.player!!.id
                        )
                    })
                } else {
                    userTeamLiveData.value!!.playersReserves.remove(userTeamLiveData.value!!.playersReserves.first {
                        it.id.equals(
                            offer.player!!.id
                        )
                    })
                }
                userTeamLiveData.value!!.credit -= values.value
                marketLiveData.value!!.remove(marketLiveData.value!!.first { it.id.equals(offer.id) })
                gameLiveData.value!!.market.remove(gameLiveData.value!!.market.first {
                    it.id.equals(
                        offer.id
                    )
                })
                offerActionSuccessLiveData.value = 5.toByte()
            }.addOnFailureListener {
                Log.e(ContentValues.TAG, it.message, it)
                offerActionSuccessLiveData.value = 15.toByte()
            }
        } else {
            val biddingAux = mutableMapOf<String, Double>()
            biddingAux.putAll(offer.bidding)
            biddingAux.remove(values.key)
            offerRepository.rejectOffer(offer.id, biddingAux).addOnSuccessListener {
                Log.i(ContentValues.TAG, "Reject offer: Success!")
                marketLiveData.value!!.first { it.id.equals(offer.id) }.bidding = biddingAux
                offerActionSuccessLiveData.value = 6.toByte()
            }.addOnFailureListener {
                Log.e(ContentValues.TAG, it.message, it)
                offerActionSuccessLiveData.value = 16.toByte()
            }
        }
    }

    fun getPlayers(playersRef: List<String>): List<Player> {
        return localDB.selectMultiplePlayersByIds(playersRef)
    }

    private fun checkOfferFields(player: Player?, price: String): Boolean {
        val context = getApplication<Application>().applicationContext
        if (player == null || player.id.isEmpty()) {
            toastLiveData.value = context.getString(R.string.no_player_selected)
        } else if (price.isEmpty()) {
            toastLiveData.value = context.getString(R.string.no_price)
        } else {
            return true
        }
        return false
    }

    fun resetOfferSuccess() {
        offerActionSuccessLiveData.value = 0.toByte()
    }

}