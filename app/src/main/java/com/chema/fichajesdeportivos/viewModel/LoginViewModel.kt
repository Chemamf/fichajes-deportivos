package com.chema.fichajesdeportivos.viewModel

import android.app.Application
import android.content.ContentValues
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.chema.fichajesdeportivos.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.auth.FirebaseUser

class LoginViewModel(application: Application) : AndroidViewModel(application) {

    private val auth: FirebaseAuth = FirebaseAuth.getInstance()

    val userAccountLiveData = MutableLiveData<FirebaseUser>()
    val toastLiveData = MutableLiveData<String>()

    fun login(username: String, password: String) {
        val context = getApplication<Application>().applicationContext
        when {
            username.isEmpty() -> {
                toastLiveData.value = context.resources.getString(R.string.empty_email)
            }
            password.isEmpty() -> {
                toastLiveData.value = context.resources.getString(R.string.empty_password)
            }
            else -> {
                auth.signInWithEmailAndPassword(username, password)
                    .addOnCompleteListener() { task ->
                        if (task.isSuccessful) {
                            Log.i(ContentValues.TAG, "signInWithEmail:success")
                            userAccountLiveData.value = auth.currentUser
                        } else {
                            try {
                                throw task.exception!!
                            } catch (t: FirebaseAuthInvalidCredentialsException) {
                                Log.w(
                                    ContentValues.TAG,
                                    "signInWithEmail: " + context.resources.getString(R.string.wrong_password),
                                    task.exception
                                )
                                toastLiveData.value =
                                    context.resources.getString(R.string.wrong_password)
                            } catch (t: FirebaseAuthInvalidUserException) {
                                Log.w(
                                    ContentValues.TAG,
                                    "signInWithEmail: " + context.resources.getString(R.string.user_not_exist),
                                    task.exception
                                )
                                toastLiveData.value =
                                    context.resources.getString(R.string.user_not_exist)
                            } catch (t: Throwable) {
                                Log.w(ContentValues.TAG, "signInWithEmail:failure", task.exception)
                                toastLiveData.value =
                                    context.resources.getString(R.string.authentication_failed)
                            }
                        }
                    }
            }
        }
    }
}