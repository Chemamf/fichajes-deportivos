package com.chema.fichajesdeportivos.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.enum.Formation
import com.chema.fichajesdeportivos.viewModel.GameViewModel

class DropdownFormationsAdapter(
    context: Context,
    private val gameViewModel: GameViewModel
) : ArrayAdapter<Formation>(
    context,
    R.layout.spinner_dropdown_item_center_custom,
    Formation.values()
) {
    @SuppressLint("ViewHolder", "InflateParams")
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(context)
        val rowView = inflater.inflate(R.layout.spinner_dropdown_item_center_custom, null, true)

        val formation = rowView.findViewById(android.R.id.text1) as TextView

        formation.text = Formation.values()[position].formation
        formation.setOnClickListener {
            gameViewModel.changeFormation(Formation.values()[position])
        }
        return rowView
    }
}