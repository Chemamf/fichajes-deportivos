package com.chema.fichajesdeportivos.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.Player
import com.chema.fichajesdeportivos.other.Utils

class ListAllPlayersAdapter(
    context: Context,
    private val players: List<Player>
) : ArrayAdapter<Player>(context, R.layout.listview_item_myteam_all_players, players) {
    @SuppressLint("ViewHolder", "InflateParams")
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(context)
        val rowView = inflater.inflate(R.layout.listview_item_myteam_all_players, null, true)

        val name = rowView.findViewById(R.id.myTeamAllPlayerName) as TextView
        val playerPosition = rowView.findViewById(R.id.myTeamAllPlayerPosition) as TextView
        val value = rowView.findViewById(R.id.myTeamAllPlayerValue) as TextView
        val points = rowView.findViewById(R.id.myTeamAllPlayerPoints) as TextView

        name.text = players[position].name
        playerPosition.text = context.getString(
            R.string.player_position,
            players[position].position.name
        )
        value.text = context.getString(
            R.string.player_value,
            Utils.valueFormat(players[position].value.toLong())
        )
        points.text = context.getString(
            R.string.match_info_total_points,
            players[position].totalPoints.toString()
        )
        if (players[position].totalPoints > 0) {
            points.setTextColor(ContextCompat.getColor(context, R.color.green))
        } else if (players[position].totalPoints < 0) {
            points.setTextColor(ContextCompat.getColor(context, R.color.red))
        }
        return rowView
    }

}