package com.chema.fichajesdeportivos.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.Player
import com.chema.fichajesdeportivos.model.enum.Position
import com.chema.fichajesdeportivos.viewModel.GameViewModel

class DropdownMyTeamReservesAdapter(
    context: Context,
    private val playerPosition: Position,
    private val gameViewModel: GameViewModel
) : ArrayAdapter<Player>(
    context,
    R.layout.spinner_dropdown_item_autocomplete_custom,
    gameViewModel.onEditUserTeamLiveData.value!!.third[playerPosition]!!
) {
    @SuppressLint("ViewHolder", "InflateParams")
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(context)
        val rowView = inflater.inflate(R.layout.spinner_dropdown_item_autocomplete_custom, null, true)
        val players =  gameViewModel.onEditUserTeamLiveData.value!!.third[playerPosition]!!

        val name = rowView.findViewById(android.R.id.text1) as TextView

        name.text = players[position].name
        name.setOnClickListener {
            gameViewModel.changePlayer(players[position], true)
        }
        return rowView
    }
}