package com.chema.fichajesdeportivos.adapters

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.chema.fichajesdeportivos.R

class ListSubItemAdapter(
    context: Context,
    private val item: List<String>,
    private val subItem: List<String>
) : ArrayAdapter<String>(context, R.layout.listview_item_subitem_custom, item) {
    @SuppressLint("ViewHolder", "InflateParams")
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(context)
        val rowView = inflater.inflate(R.layout.listview_item_subitem_custom, null, true)

        val itemText = rowView.findViewById(android.R.id.text1) as TextView
        val subItemText = rowView.findViewById(android.R.id.text2) as TextView

        itemText.text = item[position]
        subItemText.text = subItem[position]

        return rowView
    }
}