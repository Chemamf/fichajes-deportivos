package com.chema.fichajesdeportivos.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.Offer
import com.chema.fichajesdeportivos.model.UserProfile
import com.chema.fichajesdeportivos.other.Utils
import com.chema.fichajesdeportivos.viewModel.GameViewModel
import com.google.android.material.button.MaterialButton
import com.google.firebase.firestore.auth.User

class ListOfferBiddingAdapter(
    context: Context,
    private val offer: Offer,
    private val users: List<UserProfile>,
    private val bidding: List<Map.Entry<String, Double>>,
    private val gameViewModel: GameViewModel
) :
    ArrayAdapter<Map.Entry<String, Double>>(
        context,
        R.layout.listview_item_myteam_players,
        bidding
    ) {
    @SuppressLint("ViewHolder", "InflateParams")
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(context)
        val rowView = inflater.inflate(R.layout.listview_item_bidding_offers, null, true)

        val text = rowView.findViewById(R.id.obReceivedUserPrice) as TextView
        val denny = rowView.findViewById(R.id.obReceivedDenyButton) as MaterialButton
        val accept = rowView.findViewById(R.id.obReceivedAcceptButton) as MaterialButton

        val buyer =
            if (bidding[position].key.equals("IA")) UserProfile(username = "IA")
            else users.first { it.id.equals(bidding[position].key) }

        text.text = context.getString(
            R.string.bidding_user_price_value,
            buyer.username,
            Utils.valueFormat(bidding[position].value.toLong())
        )

        accept.setOnClickListener {
            gameViewModel.resolveOffer(bidding[position], offer, true)
        }
        denny.setOnClickListener {
            gameViewModel.resolveOffer(bidding[position], offer, false)
        }

        return rowView
    }
}