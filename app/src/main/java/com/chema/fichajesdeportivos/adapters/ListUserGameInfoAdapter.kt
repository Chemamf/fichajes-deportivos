package com.chema.fichajesdeportivos.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.Game
import com.chema.fichajesdeportivos.model.UserProfile
import com.chema.fichajesdeportivos.ui.gameActivityFragment.GameInfoFragment
import com.chema.fichajesdeportivos.viewModel.GameViewModel

class ListUserGameInfoAdapter(
    context: Context,
    private val gameViewModel: GameViewModel
) : ArrayAdapter<UserProfile>(context, R.layout.listview_item_user_game_info, gameViewModel.usersLiveData.value!!) {
    @SuppressLint("ViewHolder", "InflateParams")
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(context)
        val rowView = inflater.inflate(R.layout.listview_item_user_game_info, null, true)
        val users = gameViewModel.usersLiveData.value!!
        val game = gameViewModel.gameLiveData.value!!

        val username = rowView.findViewById(R.id.gameInfoSpinnerUsername) as TextView
        val icon = rowView.findViewById(R.id.gameInfoSpinnerIcon) as ImageView

        val user = users[position]
        username.text = user.username
        if (user.id.equals(game.administrator!!.id)) {
            icon.setImageResource(R.drawable.ic_baseline_star_24)
            icon.isEnabled = false
            rowView.isEnabled = false
        } else if (game.administrator!!.id.equals(gameViewModel.userLiveData.value!!.id)) {
            icon.setImageResource(R.drawable.ic_baseline_remove_circle_24)
            icon.setOnClickListener {
                gameViewModel.userToDeleteFromGamePositionLiveData.value = position
            }
        }
        return rowView
    }
}