package com.chema.fichajesdeportivos.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.Player

class ListPlayerAdapter(
    context: Context,
    private val players: List<Player>
) : ArrayAdapter<Player>(context, R.layout.listview_item_player_custom, players) {
    @SuppressLint("ViewHolder", "InflateParams")
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(context)
        val rowView = inflater.inflate(R.layout.listview_item_player_custom, null, true)

        val name = rowView.findViewById(R.id.playerItemName) as TextView
        val lastMatchPoints = rowView.findViewById(R.id.playerItemLMPoints) as TextView
        val totalPoints = rowView.findViewById(R.id.playerIteamTotalPoints) as TextView

        name.text = players[position].name
        if(players[position].pointsLastMatch == 0){
            lastMatchPoints.setTextColor(ContextCompat.getColor(context, R.color.grayish_white))
        } else if(players[position].pointsLastMatch < 0){
            lastMatchPoints.setTextColor(ContextCompat.getColor(context, R.color.red))
        }
        lastMatchPoints.text = context.getString(
            R.string.count_points,
            players[position].pointsLastMatch.toString()
        )
        totalPoints.text = context.getString(
            R.string.match_info_total_points,
            players[position].totalPoints.toString()
        )

        return rowView
    }

}