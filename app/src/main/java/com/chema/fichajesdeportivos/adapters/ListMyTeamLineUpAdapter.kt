package com.chema.fichajesdeportivos.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.Player
import com.chema.fichajesdeportivos.model.enum.Position
import com.chema.fichajesdeportivos.viewModel.GameViewModel

class ListMyTeamLineUpAdapter(
    context: Context,
    private val playerPosition: Position,
    private val gameViewModel: GameViewModel
) : ArrayAdapter<Player>(
    context,
    R.layout.listview_item_myteam_players,
    gameViewModel.onEditUserTeamLiveData.value!!.second[playerPosition]!!
) {
    @SuppressLint("ViewHolder", "InflateParams")
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(context)
        val rowView = inflater.inflate(R.layout.listview_item_myteam_players, null, true)
        val players =  gameViewModel.onEditUserTeamLiveData.value!!.second[playerPosition]!!

        val name = rowView.findViewById(R.id.myTeamPlayerName) as TextView
        val icon = rowView.findViewById(R.id.myTeamRemovePlayer) as ImageView

        name.text = players[position].name
        icon.setOnClickListener {
            gameViewModel.changePlayer(players[position], false)
        }
        return rowView
    }
}