package com.chema.fichajesdeportivos.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.Offer
import com.chema.fichajesdeportivos.model.Player
import com.chema.fichajesdeportivos.model.UserProfile
import com.chema.fichajesdeportivos.other.Utils
import com.chema.fichajesdeportivos.viewModel.GameViewModel
import kotlinx.android.synthetic.main.fragment_market.*

class ListOffersReceivedAdapter(
    context: Context,
    private val offers: List<Offer>,
    private val players: List<Player>,
    private val users: List<UserProfile>,
    private val gameViewModel: GameViewModel
) : ArrayAdapter<Offer>(
    context,
    R.layout.listview_item_myteam_players,
    offers
) {
    @SuppressLint("ViewHolder", "InflateParams")
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(context)
        val rowView = inflater.inflate(R.layout.listview_item_offers_received, null, true)

        val name = rowView.findViewById(R.id.oReceivedName) as TextView
        val price = rowView.findViewById(R.id.oReceivedPrice) as TextView
        val oPosition = rowView.findViewById(R.id.oReceivedPosition) as TextView
        val value = rowView.findViewById(R.id.oReceivedValue) as TextView
        val points = rowView.findViewById(R.id.oReceivedPoints) as TextView
        val biddingList = rowView.findViewById(R.id.oReceivedBidding) as ListView

        val player = players.first { it.id.equals(offers[position].player!!.id) }

        name.text = player.name
        price.text = context.getString(
            R.string.price_value,
            Utils.valueFormat(offers[position].price.toLong())
        )
        oPosition.text = context.getString(R.string.player_position, player.position.name)
        value.text =
            context.getString(R.string.player_value, Utils.valueFormat(player.value.toLong()))
        points.text =
            context.getString(R.string.match_info_total_points, player.totalPoints.toString())
        if (offers[position].bidding.entries.toList().isNotEmpty()) {
            biddingList.adapter = ListOfferBiddingAdapter(
                context,
                offers[position],
                users,
                offers[position].bidding.entries.toList(),
                gameViewModel
            )

            val row1 = biddingList.adapter.getView(0, null, biddingList)
            row1.measure(
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
            )
            biddingList.layoutParams.height =
                (row1.measuredHeight * (biddingList.count + 0.2)).toInt()
        }

        return rowView
    }
}