package com.chema.fichajesdeportivos.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.Offer
import com.chema.fichajesdeportivos.model.Player
import com.chema.fichajesdeportivos.model.RealTeam
import com.chema.fichajesdeportivos.model.UserProfile
import com.chema.fichajesdeportivos.other.Utils
import com.chema.fichajesdeportivos.viewModel.GameViewModel
import com.google.firebase.firestore.FirebaseFirestore

class ListOffersAdapter(
    context: Context,
    private val offers: List<Offer>,
    private val players: List<Player>,
    private val realTeams: List<RealTeam>,
    private val gameViewModel: GameViewModel
) : ArrayAdapter<Offer>(context, R.layout.listview_item_offers, offers) {
    @SuppressLint("ViewHolder", "InflateParams")
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(context)
        val rowView = inflater.inflate(R.layout.listview_item_offers, null, true)
        val playerRef = FirebaseFirestore.getInstance().collection("Players")

        val name = rowView.findViewById(R.id.offersPlayerName) as TextView
        val team = rowView.findViewById(R.id.offersPlayerTeam) as TextView
        val playerPosition = rowView.findViewById(R.id.offersPlayerPosition) as TextView
        val value = rowView.findViewById(R.id.offersPlayerValue) as TextView
        val price = rowView.findViewById(R.id.offersPrice) as TextView
        val points = rowView.findViewById(R.id.offersPlayerPoints) as TextView
        val owner = rowView.findViewById(R.id.offersPlayerOwner) as TextView

        val user = if (offers[position].seller!!.id.equals("IA")) UserProfile(
            id = "IA",
            username = "IA",
            userAccountUID = "IA"
        ) else {
            gameViewModel.usersLiveData.value!!.first { it.id.equals(offers[position].seller!!.id) }
        }
        val player = players.first{ it.id.equals(offers[position].player!!.id)}
        val realTeam = realTeams.first {
            it.players.contains(playerRef.document(player.id))
        }

        name.text = player.name

        team.text = context.getString(R.string.name_team_value, realTeam.longName)

        playerPosition.text = context.getString(
            R.string.player_position,
            player.position.name
        )
        value.text = context.getString(
            R.string.player_value,
            Utils.valueFormat(player.value.toLong())
        )
        price.text = context.getString(
            R.string.price_value,
            Utils.valueFormat(offers[position].price.toLong())
        )
        points.text = context.getString(
            R.string.match_info_total_points,
            player.totalPoints.toString()
        )
        owner.text = context.getString(
            R.string.owner_value,
            user.username
        )
        if (user.id.equals(gameViewModel.userLiveData.value!!.id)) {
            owner.setTypeface(null, Typeface.BOLD)
            owner.setTextColor(ContextCompat.getColor(context, R.color.yellow))
            rowView.setOnClickListener {
                gameViewModel.offerPlayerLiveData.value =
                    Triple(false, player, offers[position].copy())
            }
        } else {
            rowView.setOnClickListener {
                gameViewModel.offerPlayerLiveData.value =
                    Triple(true, player, offers[position].copy())
            }
        }
        return rowView
    }

}