package com.chema.fichajesdeportivos.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.chema.fichajesdeportivos.model.enum.Formation

class DropdownCustomAdapter(
    context: Context,
    private val values: List<String>,
    private val layout: Int
) : ArrayAdapter<String>(context, layout, values) {
    @SuppressLint("ViewHolder", "InflateParams")
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(context)
        val rowView = inflater.inflate(layout, null, true)
        val text = rowView.findViewById(android.R.id.text1) as TextView

        text.text = values[position]

        return rowView
    }
}