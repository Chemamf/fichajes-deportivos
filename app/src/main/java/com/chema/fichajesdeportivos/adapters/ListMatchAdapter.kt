package com.chema.fichajesdeportivos.adapters

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.chema.fichajesdeportivos.R
import com.chema.fichajesdeportivos.model.Match
import com.chema.fichajesdeportivos.model.RealTeam

class ListMatchAdapter(
    context: Context,
    private val matches: List<Match>,
    private val teams: List<RealTeam>
) : ArrayAdapter<Match>(context, R.layout.listview_item_match_custom, matches) {
    @SuppressLint("ViewHolder", "InflateParams", "SetTextI18n")
    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(context)
        val rowView = inflater.inflate(R.layout.listview_item_match_custom, null, true)

        val localTeam = rowView.findViewById(R.id.localTeam) as TextView
        val result = rowView.findViewById(R.id.result) as TextView
        val awayTeam = rowView.findViewById(R.id.awayTeam) as TextView

        localTeam.text = teams.first { it.id.equals(matches[position].homeTeam!!.id) }.longName
        result.text =
            matches[position].homeTeamGoals.toString() + " - " + matches[position].awayTeamGoals.toString()
        awayTeam.text = teams.first { it.id.equals(matches[position].awayTeam!!.id) }.longName

        return rowView
    }
}