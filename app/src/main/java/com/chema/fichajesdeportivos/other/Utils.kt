package com.chema.fichajesdeportivos.other

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentValues
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.reflect.KProperty
import kotlin.reflect.full.memberProperties
import kotlin.reflect.typeOf

object Utils {
    fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view: View? = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun idListToDocRefList(
        ids: List<String>,
        collection: CollectionReference
    ): MutableList<DocumentReference> {
        val docRefs = mutableListOf<DocumentReference>()
        for (id in ids) {
            docRefs.add(collection.document(id))
        }
        return docRefs
    }

    fun docRefListToIdList(docRefs: List<DocumentReference>): MutableList<String> {
        val ids = mutableListOf<String>()
        for (docRef in docRefs) {
            ids.add(docRef.id)
        }
        return ids
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : Any> deserializeDataClassWithDocRefs(obj: T): Map<String, List<String>> {
        val result = mutableMapOf<String, List<String>>()
        for (prop in obj::class.memberProperties) {
            if (prop is DocumentReference) {
                result[prop.name] = listOf(prop.id)
            } else if (prop is List<*>) {
                if (prop[0] is DocumentReference) {
                    result[prop.name] = docRefListToIdList(prop as List<DocumentReference>)
                }
            }
        }
        return result
    }

    fun <T : Any> objectListToDocRefList(
        objects: List<T>,
        collection: CollectionReference
    ): List<DocumentReference> {
        val result = mutableListOf<DocumentReference>()
        for (o in objects) {
            result.add(collection.document(o.javaClass.kotlin.memberProperties.first { it.name == "id" }
                .get(o).toString()))
        }
        return result
    }

    fun <T : Any> objectListToStringList(objects: List<T>, ): List<String> {
        val result = mutableListOf<String>()
        for (o in objects) {
            result.add(o.javaClass.kotlin.memberProperties.first { it.name == "id" }
                .get(o).toString())
        }
        return result
    }

    fun valueFormat(v: Long): String {
        var s = v.toString()
        if (v < 1000L) {
            return s
        } else if (v < 1000000L) {
            s = s.dropLast(3)
            s += "K"
        } else {
            s = s.dropLast(5)
            s = s.substring(0, s.length - 1) + "," + s.substring(s.length - 1)
            var aux = s.substring(0, s.length - 2)
            var aux2 = ""
            while (aux.length > 3) {
                aux2 = "." + aux.substring(aux.length - 3) + aux2
                aux = aux.dropLast(3)
            }
            if (aux2.isNotEmpty())
                s = aux + aux2 + s.substring(s.length - 2) + "M"
            else
                s += "M"
        }
        return s
    }

    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    fun valueAndSizeToDouble(value: String, size: String) : Double?{
        var result: Double? = null
        try {
            result = if (!value.contains(",")) {
                value.toDouble()
            } else {
                NumberFormat.getInstance(Locale.FRANCE).parse(value).toDouble()
            }
        } catch (t: Throwable) {
            Log.e(ContentValues.TAG, t.message, t)
            return result
        }
        val aux = DecimalFormat("#.0").format(result)
        result = NumberFormat.getInstance(Locale.FRANCE).parse(aux).toDouble()
        return if (size.equals("K")) result * 1000 else result * 1000000
    }

}
