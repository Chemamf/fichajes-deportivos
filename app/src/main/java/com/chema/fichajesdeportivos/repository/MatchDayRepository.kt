package com.chema.fichajesdeportivos.repository

import com.chema.fichajesdeportivos.model.MatchDay
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore

class MatchDayRepository {
    private val db = FirebaseFirestore.getInstance()
    private val matchDayRef = db.collection("MatchDays")
    private val systemVariablesRef = db.collection("SystemVariables")


    fun getMatchDay(id: String): Task<MatchDay> {
        return matchDayRef.document(id).get().continueWith {
            it.result!!.toObject(MatchDay::class.java)
        }
    }

    /** Devuelve un Pair con el matchday anterior y el actual respectivamente*/
    @Suppress("UNCHECKED_CAST")
    fun getCurrentAndLastMatchDay(): Task<Pair<MatchDay, MatchDay>> {
        return db.runTransaction {
            val currentMatchday =
                it.get(systemVariablesRef.document("currentMatchDay"))["value"].toString()
            return@runTransaction Pair(
                it.get(matchDayRef.document((currentMatchday.toInt() - 1).toString()))
                    .toObject(MatchDay::class.java),
                it.get(matchDayRef.document(currentMatchday)).toObject(MatchDay::class.java)
            ) as Pair<MatchDay, MatchDay>
        }
    }

    fun getAllMatchDays(): Task<List<MatchDay>> {
        return matchDayRef.get().continueWith {
            it.result!!.toObjects(MatchDay::class.java)
        }
    }

    companion object {
        val instance = MatchDayRepository()
    }
}