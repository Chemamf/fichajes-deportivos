package com.chema.fichajesdeportivos.repository

import com.chema.fichajesdeportivos.model.Game
import com.chema.fichajesdeportivos.model.Player
import com.chema.fichajesdeportivos.model.UserTeam
import com.chema.fichajesdeportivos.model.enum.Formation
import com.chema.fichajesdeportivos.model.enum.Position
import com.chema.fichajesdeportivos.other.Utils
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore

class
UserTeamRepository {
    private val db = FirebaseFirestore.getInstance()
    private val userTeamRef = db.collection("UserTeams")

    fun create(userTeam: UserTeam): Task<DocumentReference> {
        return userTeamRef.add(userTeam)
    }

    fun getCollectionReference(): CollectionReference {
        return userTeamRef
    }

    fun delete(id: String): Task<Void> {
        return userTeamRef.document(id).delete()
    }

    fun getUserTeamsIdsByUser(user: DocumentReference): Task<List<String>> {
        return userTeamRef.whereEqualTo("userProfile", user).get().continueWith {
            val result = mutableListOf<String>()
            for (r in it.result!!) {
                result.add(r.id)
            }
            return@continueWith result
        }
    }

    fun updateLineUpAndFormation(
        userTeam: UserTeam
    ): Task<Void> {
        return userTeamRef.document(userTeam.id).update(
            mapOf(
                "formation" to userTeam.formation,
                "playersLineUp" to userTeam.playersLineUp,
                "playersReserves" to userTeam.playersReserves
            )
        )
    }


    companion object {
        val instance = UserTeamRepository()
    }
}