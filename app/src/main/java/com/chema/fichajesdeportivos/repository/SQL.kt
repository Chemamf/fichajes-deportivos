package com.chema.fichajesdeportivos.repository

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import android.util.Log
import com.chema.fichajesdeportivos.model.Match
import com.chema.fichajesdeportivos.model.Player
import com.chema.fichajesdeportivos.model.RealTeam
import com.chema.fichajesdeportivos.model.enum.Position
import com.chema.fichajesdeportivos.other.Utils
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore


object RealTeamsSQL : BaseColumns {
    const val TABLE_NAME = "realTeams"
    const val COLUMN_NAME = "name"
    const val COLUMN_LONGNAME = "longName"
    const val COLUMN_PLAYERS = "players"
}

object PlayersSQL : BaseColumns {
    const val TABLE_NAME = "players"
    const val COLUMN_NAME = "name"
    const val COLUMN_POINTS_LAST_MATCH = "pointsLastMatch"
    const val COLUMN_POSITION = "position"
    const val COLUMN_TOTAL_POINTS = "totalPoints"
    const val COLUMN_VALUE = "value"
}

object MatchesSQL : BaseColumns {
    const val TABLE_NAME = "matches"
    const val COLUMN_HOME_TEAM = "homeTeam"
    const val COLUMN_AWAY_TEAM = "awayTeam"
    const val COLUMN_HOME_TEAM_GOALS = "homeTeamGoals"
    const val COLUMN_AWAY_TEAM_GOALS = "awayTeamGoals"
}

private const val SQL_CREATE_TABLE_REALTEAMS =
    "CREATE TABLE IF NOT EXISTS ${RealTeamsSQL.TABLE_NAME}(" +
            "${BaseColumns._ID} TEXT PRIMARY KEY," +
            "${RealTeamsSQL.COLUMN_NAME} TEXT," +
            "${RealTeamsSQL.COLUMN_LONGNAME} TEXT," +
            "${RealTeamsSQL.COLUMN_PLAYERS} TEXT)"

private const val SQL_DROP_TABLE_REALTEAMS =
    "DROP TABLE IF EXISTS ${RealTeamsSQL.TABLE_NAME}"


private const val SQL_CREATE_TABLE_PLAYERS =
    "CREATE TABLE IF NOT EXISTS ${PlayersSQL.TABLE_NAME}(" +
            "${BaseColumns._ID} TEXT PRIMARY KEY," +
            "${PlayersSQL.COLUMN_NAME} TEXT," +
            "${PlayersSQL.COLUMN_POINTS_LAST_MATCH} INTEGER," +
            "${PlayersSQL.COLUMN_POSITION} TEXT," +
            "${PlayersSQL.COLUMN_TOTAL_POINTS} INTEGER," +
            "${PlayersSQL.COLUMN_VALUE} INTEGER)"

private const val SQL_DROP_TABLE_PLAYERS =
    "DROP TABLE IF EXISTS ${PlayersSQL.TABLE_NAME}"


private const val SQL_CREATE_TABLE_MATCHES =
    "CREATE TABLE IF NOT EXISTS ${MatchesSQL.TABLE_NAME}(" +
            "${BaseColumns._ID} TEXT PRIMARY KEY," +
            "${MatchesSQL.COLUMN_HOME_TEAM} TEXT," +
            "${MatchesSQL.COLUMN_AWAY_TEAM} TEXT," +
            "${MatchesSQL.COLUMN_HOME_TEAM_GOALS} INTEGER," +
            "${MatchesSQL.COLUMN_AWAY_TEAM_GOALS} INTEGER)"

private const val SQL_DROP_TABLE_MATCHES =
    "DROP TABLE IF EXISTS ${MatchesSQL.TABLE_NAME}"


class DbHelper(context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_TABLE_REALTEAMS)
        db.execSQL(SQL_CREATE_TABLE_PLAYERS)
        db.execSQL(SQL_CREATE_TABLE_MATCHES)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL(SQL_DROP_TABLE_REALTEAMS)
        db.execSQL(SQL_DROP_TABLE_PLAYERS)
        db.execSQL(SQL_DROP_TABLE_MATCHES)
        onCreate(db)
    }

    companion object {
        // If you change the database schema, you must increment the database version.
        const val DATABASE_VERSION = 3
        const val DATABASE_NAME = "database.db"
    }
}

class DB(context: Context) {
    private val dbHelper = DbHelper(context)

    //###########################################
    // Matches
    //###########################################
    fun insertOrUpdateMatches(data: List<Match>) {
        val db = dbHelper.writableDatabase
        db.beginTransaction()
        for (match in data) {
            val values = ContentValues().apply {
                put(BaseColumns._ID, match.id)
                put(MatchesSQL.COLUMN_HOME_TEAM, match.homeTeam!!.id)
                put(MatchesSQL.COLUMN_AWAY_TEAM, match.awayTeam!!.id)
                put(MatchesSQL.COLUMN_HOME_TEAM_GOALS, match.homeTeamGoals)
                put(MatchesSQL.COLUMN_AWAY_TEAM_GOALS, match.awayTeamGoals)
            }
            db.insert(MatchesSQL.TABLE_NAME, null, values)
        }
        db.setTransactionSuccessful()
        db.endTransaction()
    }

    fun selectMatchesByMatchday(matchesId: List<String>): List<Match> {
        val db = dbHelper.readableDatabase
        val idsQuery = listToInArg(matchesId)
        val cursor = db.rawQuery(
            "SELECT * FROM ${MatchesSQL.TABLE_NAME} WHERE ${BaseColumns._ID} IN $idsQuery",
            null
        )
        val matches = cursorToMatchesList(cursor)
        cursor.close()
        return matches

    }

    //###########################################
    // Matches - Métodos Auxiliares
    //###########################################
    private fun cursorToMatchesList(cursor: Cursor): List<Match> {
        val matchesCollection = FirebaseFirestore.getInstance().collection("Matches")
        val matches = mutableListOf<Match>()
        with(cursor) {
            while (moveToNext()) {
                val match = Match(
                    getString(getColumnIndexOrThrow(BaseColumns._ID)),
                    getInt(getColumnIndexOrThrow(MatchesSQL.COLUMN_HOME_TEAM_GOALS)),
                    getInt(getColumnIndexOrThrow(MatchesSQL.COLUMN_AWAY_TEAM_GOALS)),
                    matchesCollection.document(getString(getColumnIndexOrThrow(MatchesSQL.COLUMN_HOME_TEAM))),
                    matchesCollection.document(getString(getColumnIndexOrThrow(MatchesSQL.COLUMN_AWAY_TEAM)))
                )
                matches.add(match)
            }
        }
        return matches
    }

    //###########################################
    // Teams
    //###########################################
    fun insertOrUpdateRealTeams(data: List<RealTeam>) {
        val db = dbHelper.writableDatabase
        db.beginTransaction()
        for (team in data) {
            val players = team.players.map { documentReference -> documentReference.id }
            val values = ContentValues().apply {
                put(BaseColumns._ID, team.id)
                put(RealTeamsSQL.COLUMN_NAME, team.name)
                put(RealTeamsSQL.COLUMN_LONGNAME, team.longName)
                put(RealTeamsSQL.COLUMN_PLAYERS, players.joinToString(","))
            }
            db.insert(RealTeamsSQL.TABLE_NAME, null, values)
        }
        db.setTransactionSuccessful()
        db.endTransaction()
    }

    fun selectAllRealTeams(): List<RealTeam> {
        val db = dbHelper.readableDatabase
        val cursor = db.query(
            RealTeamsSQL.TABLE_NAME, null, null, null, null, null, null
        )
        val realTeams = cursorToRealTeamList(cursor)
        cursor.close()
        return realTeams
    }

    fun selectRealTeamsByIdList(ids: List<String>): List<RealTeam> {
        val db = dbHelper.readableDatabase
        val idsQuery = listToInArg(ids)
        val cursor = db.rawQuery(
            "SELECT * FROM ${RealTeamsSQL.TABLE_NAME} WHERE ${BaseColumns._ID} IN $idsQuery",
            null
        )
        val realTeams = cursorToRealTeamList(cursor)
        cursor.close()
        return realTeams
    }

    //###########################################
    // RealTeams - Métodos Auxiliares
    //###########################################
    private fun cursorToRealTeamList(cursor: Cursor): List<RealTeam> {
        val realTeams = mutableListOf<RealTeam>()
        with(cursor) {
            while (moveToNext()) {
                val realTeam = RealTeam(
                    getString(getColumnIndexOrThrow(BaseColumns._ID)),
                    getString(getColumnIndexOrThrow(RealTeamsSQL.COLUMN_NAME)),
                    getString(getColumnIndexOrThrow(RealTeamsSQL.COLUMN_LONGNAME)),
                    Utils.idListToDocRefList(
                        sqlStringListToStringList(
                            getString(getColumnIndexOrThrow(RealTeamsSQL.COLUMN_PLAYERS))
                        ), FirebaseFirestore.getInstance().collection("Players")
                    )
                )
                realTeams.add(realTeam)
            }
        }
        return realTeams
    }

    //###########################################
    // Players
    //###########################################
    fun insertOrUpdatePlayers(data: List<Player>) {
        val db = dbHelper.writableDatabase
        db.beginTransaction()
        for (player in data) {
            val values = ContentValues().apply {
                put(BaseColumns._ID, player.id)
                put(PlayersSQL.COLUMN_NAME, player.name)
                put(PlayersSQL.COLUMN_TOTAL_POINTS, player.totalPoints)
                put(PlayersSQL.COLUMN_POINTS_LAST_MATCH, player.pointsLastMatch)
                put(PlayersSQL.COLUMN_POSITION, player.position.name)
                put(PlayersSQL.COLUMN_VALUE, player.value)
            }
            db.insert(PlayersSQL.TABLE_NAME, null, values)
        }
        db.setTransactionSuccessful()
        db.endTransaction()
    }

    fun selectAllPlayers(): List<Player> {
        val db = dbHelper.readableDatabase
        val cursor = db.query(
            PlayersSQL.TABLE_NAME, null, null, null, null, null, null
        )
        val players = cursorToPlayerList(cursor)
        cursor.close()
        return players
    }

    fun selectRandomPlayersUserTeam(idsTaken: List<String>?, isUserTeam: Boolean): List<String> {
        val db = dbHelper.writableDatabase
        val query = if (idsTaken == null) {
            "SELECT ${BaseColumns._ID} FROM ${PlayersSQL.TABLE_NAME} WHERE ${PlayersSQL.COLUMN_VALUE} < ? " +
                    "AND ${PlayersSQL.COLUMN_POSITION} = ? ORDER BY RANDOM() LIMIT ?"
        } else if (isUserTeam) {
            val idsTakenFormatted = listToInArg(idsTaken)
            "SELECT ${BaseColumns._ID} FROM ${PlayersSQL.TABLE_NAME} WHERE ${PlayersSQL.COLUMN_VALUE} < ? " +
                    "AND ${PlayersSQL.COLUMN_POSITION} = ?" +
                    "AND ${BaseColumns._ID} NOT IN $idsTakenFormatted ORDER BY RANDOM() LIMIT ?"

        } else {
            val idsTakenFormatted = listToInArg(idsTaken)
            "SELECT ${BaseColumns._ID} FROM ${PlayersSQL.TABLE_NAME} " +
                    "WHERE ${PlayersSQL.COLUMN_POSITION} = ? " +
                    "AND ${BaseColumns._ID} NOT IN $idsTakenFormatted ORDER BY RANDOM() LIMIT ?"
        }
        val result = mutableListOf<String>()

        var cursor: Cursor = if (isUserTeam)
            db.rawQuery(query, arrayOf("2000000", Position.GOALKEEPER.name, "2"))
        else
            db.rawQuery(query, arrayOf(Position.GOALKEEPER.name, "3"))
        result.addAll(cursorToIdList(cursor))

        cursor = if (isUserTeam)
            db.rawQuery(query, arrayOf("2000000", Position.DEFENDER.name, "6"))
        else
            db.rawQuery(query, arrayOf(Position.DEFENDER.name, "5"))
        result.addAll(cursorToIdList(cursor))

        cursor = if (isUserTeam)
            db.rawQuery(query, arrayOf("2000000", Position.MIDFIELDER.name, "6"))
        else
            db.rawQuery(query, arrayOf(Position.MIDFIELDER.name, "5"))
        result.addAll(cursorToIdList(cursor))

        cursor = if (isUserTeam)
            db.rawQuery(query, arrayOf("2000000", Position.ATTACKER.name, "4"))
        else
            db.rawQuery(query, arrayOf(Position.ATTACKER.name, "4"))
        result.addAll(cursorToIdList(cursor))

        cursor.close()
        return result
    }

    fun selectMultiplePlayersByIds(ids: List<String>): List<Player> {
        val db = dbHelper.readableDatabase
        val idsQuery = listToInArg(ids)
        if (idsQuery.isNullOrEmpty()) {
            return mutableListOf()
        }
        val cursor = db.rawQuery(
            "SELECT * FROM ${PlayersSQL.TABLE_NAME} WHERE ${BaseColumns._ID} IN $idsQuery",
            null
        )
        val players = cursorToPlayerList(cursor)
        cursor.close()
        return players
    }


    //###########################################
    // Players - Métodos Auxiliares
    //###########################################
    private fun cursorToPlayerList(cursor: Cursor): List<Player> {
        val players = mutableListOf<Player>()
        with(cursor) {
            while (moveToNext()) {
                val player = Player(
                    getString(getColumnIndexOrThrow(BaseColumns._ID)),
                    getString(getColumnIndexOrThrow(PlayersSQL.COLUMN_NAME)),
                    getInt(getColumnIndexOrThrow(PlayersSQL.COLUMN_TOTAL_POINTS)),
                    getInt(getColumnIndexOrThrow(PlayersSQL.COLUMN_POINTS_LAST_MATCH)),
                    Position.valueOf(getString(getColumnIndexOrThrow(PlayersSQL.COLUMN_POSITION))),
                    getInt(getColumnIndexOrThrow(PlayersSQL.COLUMN_VALUE))
                )
                players.add(player)
            }
        }
        return players
    }

    /**Convierte una lista de Strings en una lista de strings adaptada para pasarsela a una query
     * después de un IN o NOT IN*/
    private fun cursorToIdList(cursor: Cursor): List<String> {
        val ids = mutableListOf<String>()
        with(cursor) {
            while (moveToNext()) {
                val id = getString(getColumnIndexOrThrow(BaseColumns._ID))
                ids.add(id)
            }
        }
        return ids
    }

    /**Convierte una lista de String en un String entre parentesis con cada String de la lista separado
     * por comas, para usarlo en las querys con clausulas where in o where not*/
    private fun listToInArg(strings: List<String>): String {
        if (strings.isNullOrEmpty()) {
            Log.i(ContentValues.TAG, "empty list")
            return "(0)"
        }
        var queryArg = "("
        for (s in strings) {
            queryArg += "$s,"
        }
        queryArg = queryArg.dropLast(1)
        queryArg += ")"
        return queryArg
    }

    /**Convierte una lista de Strings extraida de una columna de una tabla (strings separados por coma)
     * en una lista de Strings*/
    private fun sqlStringListToStringList(str: String): List<String> {
        return str.split(",")
    }
}