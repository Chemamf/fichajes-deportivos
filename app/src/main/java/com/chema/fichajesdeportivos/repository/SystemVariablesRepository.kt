package com.chema.fichajesdeportivos.repository

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore

class SystemVariablesRepository {
    private val db = FirebaseFirestore.getInstance()
    private val systemVariablesRef = db.collection("SystemVariables")

    fun getValue(id: String) : Task<String>{
        return systemVariablesRef.document(id).get().continueWith{
            it.result?.get("value").toString()
        }
    }

    companion object {
        val instance = SystemVariablesRepository()
    }
}