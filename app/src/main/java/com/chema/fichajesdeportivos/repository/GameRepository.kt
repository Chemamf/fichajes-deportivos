package com.chema.fichajesdeportivos.repository

import com.chema.fichajesdeportivos.model.Game
import com.chema.fichajesdeportivos.model.Offer
import com.chema.fichajesdeportivos.model.UserProfile
import com.chema.fichajesdeportivos.model.UserTeam
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.*
import com.google.firebase.installations.Utils

class GameRepository {

    private val db = FirebaseFirestore.getInstance()
    private val gameRef = db.collection("Games")
    private val userRef = db.collection("Users")
    private val userTeamRef = db.collection("UserTeams")
    private val leagueRef = db.collection("Leagues")
    private val offerRef = db.collection("Offers")


    fun getGamesByUser(userProfileId: String): Task<List<Game>> {
        return gameRef.whereArrayContains("users", userRef.document(userProfileId)).get()
            .continueWith {
                it.result?.toObjects(Game::class.java).also { result ->
                    result?.sortBy { list -> list.name }
                }
            }
    }

    fun getByName(gameName: String): Task<Game> {
        return gameRef.whereEqualTo("name", gameName).limit(1).get().continueWith {
            val docRef = it.result!!.documents[0]
            docRef.toObject(Game::class.java)
        }
    }

    fun get(gameId: String): Task<Game> {
        return gameRef.document(gameId).get().continueWith {
            it.result!!.toObject(Game::class.java)
        }
    }

    fun create(game: Game): Task<DocumentReference> {
        return gameRef.add(game)
    }

    @Suppress("UNCHECKED_CAST")
    fun getGameAndData(gameId: String): Task<Map<String, List<DocumentSnapshot>>> {
        return db.runTransaction {
            val result = mutableMapOf<String, MutableList<DocumentSnapshot>>()
            result["game"] = mutableListOf()
            result["users"] = mutableListOf()
            result["userTeams"] = mutableListOf()
            result["league"] = mutableListOf()

            val gameSnapshot = it.get(gameRef.document(gameId))
            result["game"]?.add(gameSnapshot)
            result["league"]?.add(it.get(leagueRef.document((gameSnapshot.get("league") as DocumentReference).id)))
            val users = gameSnapshot.get("users") as List<DocumentReference>
            val userTeams = gameSnapshot.get("userTeams") as List<DocumentReference>
            for (i in users.indices) {
                result["users"]?.add(it.get(userRef.document(users[i].id)))
                result["userTeams"]?.add(it.get(userTeamRef.document(userTeams[i].id)))
            }
            return@runTransaction result
        }
    }

    fun getTakenPlayersIdsByGame(game: Game): Task<List<DocumentReference>> {
        return db.runTransaction() {
            val playersId = mutableListOf<DocumentReference>()
            for (utRef in game.userTeams) {
                val ut = it.get(userTeamRef.document(utRef.id)).toObject(UserTeam::class.java)
                playersId.addAll(ut!!.playersLineUp)
                playersId.addAll(ut.playersReserves)
            }
            for (o in game.market) {
                val offer = it.get(offerRef.document(o.id)).toObject(Offer::class.java)
                playersId.add(offer?.player!!)
            }
            return@runTransaction playersId
        }
    }

    fun searchGames(key: String): Task<List<Game>> {
        return gameRef.orderBy("name").startAt(key).endAt(key + "\uf8ff").get().continueWith {
            it.result!!.toObjects(Game::class.java)
        }
    }

    fun joinGame(
        gameId: String,
        userRef: DocumentReference,
        userTeamRef: DocumentReference
    ): Task<Void> {
        return gameRef.document(gameId).update(
            mapOf(
                "users" to FieldValue.arrayUnion(userRef),
                "userTeams" to FieldValue.arrayUnion(userTeamRef)
            )
        )
    }

    fun removeUserFromGame(gameId: String, userId: String, userTeamId: String): Task<Transaction> {
        return db.runTransaction {
            val game = it.get(gameRef.document(gameId))
            it.update(
                gameRef.document(gameId),
                mapOf(
                    "users" to FieldValue.arrayRemove(userRef.document(userId)),
                    "userTeams" to FieldValue.arrayRemove(userTeamRef.document(userTeamId))
                )
            )
            it.delete(userTeamRef.document(userTeamId))
        }
    }

    fun delete(game: Game, userTeams: List<UserTeam>): Task<Void> {
        return db.runBatch {
            it.delete(gameRef.document(game.id))
            for (o in game.market) {
                it.delete(o)
            }
            for (ut in game.userTeams) {
                it.delete(ut)
            }
        }
    }


    companion object {
        val instance = GameRepository()
    }


}