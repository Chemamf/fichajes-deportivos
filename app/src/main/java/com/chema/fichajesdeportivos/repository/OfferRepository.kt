package com.chema.fichajesdeportivos.repository


import com.chema.fichajesdeportivos.model.Game
import com.chema.fichajesdeportivos.model.Offer
import com.chema.fichajesdeportivos.model.UserTeam
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.*
import com.google.firebase.firestore.auth.User

class OfferRepository {
    private val db = FirebaseFirestore.getInstance()
    private val offerRef = db.collection("Offers")
    private val gameRef = db.collection("Games")
    private val userTeamRef = db.collection("UserTeams")

    fun getCollectionReference(): CollectionReference {
        return offerRef
    }

    fun createReference(id: String): DocumentReference {
        return offerRef.document(id)
    }

    fun get(documentReference: DocumentReference): Task<Offer> {
        return documentReference.get().continueWith {
            it.result!!.toObject(Offer::class.java)
        }
    }

    fun createOffers(offers: List<Offer>, gameId: String): Task<MutableList<Offer>> {
        val docRefs = mutableListOf<DocumentReference>()
        return db.runTransaction {
            val updatedOffers = mutableListOf<Offer>()
            for (o in offers) {
                val docRef = offerRef.document()
                it.set(docRef, o)
                docRefs.add(docRef)
                updatedOffers.add(o.copy(id = docRef.id))
            }
            it.update(gameRef.document(gameId), "market", docRefs)
            return@runTransaction updatedOffers
        }
    }

    fun createOffer(offer: Offer, gameId: String): Task<Offer> {
        return db.runTransaction {
            val docRef = offerRef.document()
            it.set(docRef, offer)
            val updatedOffer = offer.copy(id = docRef.id)
            it.update(
                gameRef.document(gameId),
                "market",
                FieldValue.arrayUnion(offerRef.document(docRef.id))
            )
            return@runTransaction updatedOffer
        }
    }

    fun deleteOffer(offerId: String, gameId: String): Task<Void> {
        return db.runBatch {
            it.delete(offerRef.document(offerId))
            it.update(
                gameRef.document(gameId),
                "market",
                FieldValue.arrayRemove(offerRef.document(offerId))
            )
        }
    }

    fun updateBiddingOffer(offer: Offer): Task<Void> {
        return offerRef.document(offer.id).update("bidding", offer.bidding)
    }

    fun getOffersByGame(game: Game): Task<MutableList<Offer>> {
        return db.runTransaction {
            val result = mutableListOf<Offer>()
            for (o in game.market) {
                result.add(it.get(o).toObject(Offer::class.java)!!)
            }
            return@runTransaction result
        }
    }


    fun resolveAndDeleteOffers(userTeams: List<UserTeam>, offers: List<Offer>): Task<Void> {
        return db.runBatch {
            for (ut in userTeams) {
                it.update(
                    userTeamRef.document(ut.id),
                    mapOf(
                        "credit" to ut.credit,
                        "playersLineUp" to ut.playersLineUp,
                        "playersReserves" to ut.playersReserves
                    )
                )
            }
            for (o in offers) {
                it.delete(offerRef.document(o.id))
            }
        }
    }

    fun acceptOffer(
        buyer: UserTeam?,
        seller: UserTeam,
        offer: Offer,
        price: Double,
        gameId: String,
        playerLocation: String
    ): Task<Void> {
        return db.runBatch {
            it.update(
                userTeamRef.document(seller.id),
                playerLocation,
                FieldValue.arrayRemove(offer.player)
            )
            it.update(userTeamRef.document(seller.id), "credit", seller.credit + price)
            if (buyer != null) {
                it.update(
                    userTeamRef.document(buyer.id),
                    "playersReserves",
                    FieldValue.arrayUnion(offer.player)
                )
                it.update(userTeamRef.document(buyer.id), "credit", buyer.credit - price)
            }
            it.delete(offerRef.document(offer.id))
            it.update(
                gameRef.document(gameId),
                "market",
                FieldValue.arrayRemove(offerRef.document(offer.id))
            )
        }
    }

    fun rejectOffer(offerId: String, newBidding: Map<String, Double>): Task<Void> {
        return offerRef.document(offerId).update("bidding", newBidding)
    }

    companion object {
        val instance = OfferRepository()
    }


}