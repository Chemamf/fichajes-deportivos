package com.chema.fichajesdeportivos.repository

import android.content.ContentValues
import android.util.Log
import com.chema.fichajesdeportivos.model.Game
import com.chema.fichajesdeportivos.model.UserProfile
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore


class UserProfileRepository {

    private val db = FirebaseFirestore.getInstance()
    private val userRef = db.collection("Users")
    private val userTeamRef = db.collection("UserTeams")
    private val gameRef = db.collection("Games")

    fun getByAccount(userAccountUID: String): Task<UserProfile> {
        return userRef.whereEqualTo("userAccountUID", userAccountUID).limit(1).get().continueWith {
            val docRef = it.result!!.documents[0]
            docRef.toObject(UserProfile::class.java)
        }
    }

    fun getByUsername(username: String): Task<UserProfile> {
        return userRef.whereEqualTo("username", username).limit(1).get().continueWith {
            val docRef = it.result!!.documents[0]
            docRef.toObject(UserProfile::class.java)
        }
    }

    fun create(userProfile: UserProfile): Task<DocumentReference> {
        return userRef.add(userProfile)
    }

    fun createReference(userProfileId: String): DocumentReference {
        return userRef.document(userProfileId)
    }

    fun update(userProfile: UserProfile): Task<Void> {
        return userRef.document(userProfile.id).set(userProfile)
    }

    fun delete(userProfileId: String): Task<Void>{
        return userRef.document(userProfileId).delete()
    }

    /**Elimina el perfil del usuario, su referencia de todos sus juegos, referencias a sus equipos
     * en cada partida y todos sus equipos*/
    @Suppress("UNCHECKED_CAST")
    fun deleteUserAndAllData(
        userProfile: DocumentReference,
        games: List<Game>,
        userTeams: List<String>
    ): Task<Unit> {
        return db.runTransaction() {
            it.delete(userProfile)
            for (ut in userTeams) {
                it.delete(userTeamRef.document(ut))
            }
            for (g in games) {
                val gRef = gameRef.document(g.id)
                it.update(gRef, "users", FieldValue.arrayRemove(userProfile))
                it.update(
                    gRef, "userTeams", FieldValue.arrayRemove(selectUserTeamGame(g, userTeams))
                )
                if (g.administrator?.id.equals(userProfile.id)) {
                    if (g.userTeams.size > 1) {
                        it.update(gRef, "administrator", otherUser(userProfile, g.users))
                    } else {
                        it.delete(gameRef.document(g.id))
                        for(o in g.market){
                            it.delete(o)
                        }
                    }
                }
            }


        }
    }


    //###########################################
    // Métodos Auxiliares
    //###########################################
    /**Seleccionar otro usuario de la partida que no sea el que se está borrando*/
    private fun otherUser(
        user: DocumentReference,
        users: List<DocumentReference>
    ): DocumentReference {
        return if (users[0].id.equals(user.id)) {
            users[1]
        } else {
            users[0]
        }
    }

    /**Selecciona el userteam de la partida actual del usuario que se está borrando*/
    private fun selectUserTeamGame(game: Game, userTeams: List<String>): DocumentReference {
        for (userTeam in game.userTeams) {
            if (userTeams.contains(userTeam.id)) {
                return userTeamRef.document(userTeam.id)
            }
        }
        return userTeamRef.document("0")
    }

    companion object {
        val instance = UserProfileRepository()
    }
}