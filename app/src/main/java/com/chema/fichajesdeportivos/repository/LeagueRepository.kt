package com.chema.fichajesdeportivos.repository

import com.chema.fichajesdeportivos.model.League
import com.chema.fichajesdeportivos.model.UserProfile
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot

class LeagueRepository {

    private val db = FirebaseFirestore.getInstance()
    private val leagueRef = db.collection("Leagues")

    fun getAll(): Task<List<League>> {
        return leagueRef.get().continueWith {
            it.result?.toObjects(League::class.java)
        }
    }

    fun createReference(leagueId : String): DocumentReference {
        return leagueRef.document(leagueId)
    }


    companion object {
        val instance = LeagueRepository()
    }


}