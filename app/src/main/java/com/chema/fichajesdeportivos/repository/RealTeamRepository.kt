package com.chema.fichajesdeportivos.repository

import com.chema.fichajesdeportivos.model.RealTeam
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore

class RealTeamRepository {
    private val db = FirebaseFirestore.getInstance()
    private val realTeamRef = db.collection("RealTeams")

    fun getRealTeam(id:String) : Task<RealTeam>{
        return realTeamRef.document(id).get().continueWith {
            it.result!!.toObject(RealTeam::class.java)
        }
    }

    fun getAllRealTeams() : Task<List<RealTeam>>{
        return realTeamRef.get().continueWith {
            it.result!!.toObjects(RealTeam::class.java)
        }
    }

    companion object {
        val instance = RealTeamRepository()
    }
}