package com.chema.fichajesdeportivos.repository

import com.chema.fichajesdeportivos.model.Match
import com.chema.fichajesdeportivos.model.MatchDay
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore

class MatchRepository {
    private val db = FirebaseFirestore.getInstance()
    private val matchRef = db.collection("Matches")


    fun getMatchesByMatchDay(matchDay: MatchDay): List<Task<Match>> {
        val tasks = mutableListOf<Task<Match>>()
        for (match in matchDay.matches) {
            tasks.add(matchRef.document(match.id).get().continueWith {
                it.result!!.toObject(Match::class.java)
            })
        }
        return tasks
    }

    fun getMatch(id: String): Task<Match> {
        return matchRef.document(id).get().continueWith {
            it.result!!.toObject(Match::class.java)
        }
    }

    companion object {
        val instance = MatchRepository()
    }

}