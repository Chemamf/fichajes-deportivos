package com.chema.fichajesdeportivos.repository

import com.chema.fichajesdeportivos.model.Player
import com.chema.fichajesdeportivos.other.Utils
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore

class PlayerRepository {
    private val db = FirebaseFirestore.getInstance()
    private val playerRef = db.collection("Players")

    fun getCollectionReference(): CollectionReference{
        return playerRef
    }

    fun getPlayer(id:String): Task<Player>{
        return playerRef.document(id).get().continueWith {
            it.result!!.toObject(Player::class.java)
        }
    }

    fun getAllPlayers(): Task<List<Player>> {
        return playerRef.get().continueWith{
            it.result!!.toObjects(Player::class.java)
        }
    }

    fun idListToPlayerRefList(ids : List<String>): MutableList<DocumentReference>{
        return Utils.idListToDocRefList(ids, playerRef)
    }

    fun createReference(playerId: String) : DocumentReference{
        return playerRef.document(playerId)
    }

    companion object {
        val instance = PlayerRepository()
    }
}